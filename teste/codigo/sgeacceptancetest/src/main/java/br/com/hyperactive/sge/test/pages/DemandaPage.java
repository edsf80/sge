/**
 * 
 */
package br.com.hyperactive.sge.test.pages;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;

/**
 * @author edsf
 *
 */
public class DemandaPage extends AbstractPage {

	public DemandaPage(WebDriverProvider driverProvider) {
		super(driverProvider);
	}

	public void ehExibida() {
		$(By.xpath("html/body/div[2]/div/div/div/div[1]/div/h1")).shouldBe(visible);
	}

	public void inserirBusca(String valor) {
		$(By.xpath("html/body/div[2]/div/div/div/div[2]/div/div/div/div[1]/div[2]/div/label/input")).setValue(valor);
	}

	public boolean ehExibidaDescricaoBusca(String busca) {
		return $(By.xpath("//*[@id='dtDemanda']/tbody/tr/td[3]")).getText().equals(busca);
	}

	public boolean tabelaEstaVazia() {		
		return $(By.xpath("html/body/div[2]/div/div/div/div[2]/div/div/div/table/tbody/tr/td")).getText().equals("Nenhum item encontrado no filtro");
	}
	
	public void clicarNovaDemanda() {
		$(By.xpath("html/body/div[2]/div/div/div/div[2]/div/div/p/button")).click();
	}
	
	public void clicarSalvarDemanda() {
		$(By.xpath("html/body/div[1]/div/div/form/div[2]/input")).click();		
	}
	
	public void clicarCancelarDemanda() {
		$(By.xpath("html/body/div[1]/div/div/form/div[2]/button")).click();		
	}
	
	public void mensagemCampoDescricaoObrigatoriaEhExibida() {
		$(By.xpath("html/body/div[1]/div/div/form/div[1]/div[1]/div")).shouldBe(visible);		
	}
	
	public void mensagemCampoStatusObrigatorioEhExibida() {
		$(By.xpath("html/body/div[1]/div/div/form/div[1]/div[2]/div")).shouldBe(visible);		
	}

}
