/**
 * 
 */
package br.com.hyperactive.sge.test.pages;

import static com.codeborne.selenide.Condition.appears;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;

/**
 * @author edsf
 *
 */
public class LoginPage extends AbstractPage {

	public LoginPage(WebDriverProvider driverProvider) {
		super(driverProvider);
	}
	
	public void abrir() {
		open("http://localhost:8080/sge");
	}
	
	public void inserirLogin(String login) {
		$("#tLogin").should(appears);
		$("#tLogin").setValue(login);
	}
	
	public void inserirSenha(String senha) {
		$("#tLogin").should(appears);
		$("#tSenha").setValue(senha);
	}
	
	public void clicarLogin() {
		$("#btnLogin").click();
	}
	
	public void falhaDeValidacao() {
		$(".alert.alert-danger.ng-binding.ng-scope").shouldBe(visible);
	}
	
	public void falhaCampoSenhaVazio() {
		$(By.xpath("html/body/div[2]/div/div/div/div/div[2]/form/fieldset/div[2]/div")).shouldBe(visible);
	}
	
	public void falhaCampoLoginVazio() {
		$(By.xpath("html/body/div[2]/div/div/div/div/div[2]/form/fieldset/div[1]/div")).shouldBe(visible);
	}

}
