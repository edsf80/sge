Story:
	Como administrador
	Eu gostaria de cadastrar, atualizar e remover clientes
	Para conseguir manter o histórico de todos os meus clientes

GivenStories: br/com/hyperactive/sge/test/stories/login.story#{id6:scenario6}
	
Scenario: Abrir pagina de clientes
Given eu estou na tela principal
When clico no menu clientes
Then vai para tela de consulta de clientes

Scenario: Cadastrar cliente com sucesso
Given estou na pagina de clientes
And clico no botão novo cliente
And marco tipo de pessoa
And preencho o formulário de clientes com os seguintes campos:
|cpf|descricao|
|107.423.836-45|cliente do SIGFestas|
When clico em salvar
Then mensagem de sucesso deve aparecer

Scenario: Cadastrar cliente sem cpf
Given estou na pagina de clientes
And clico no botão novo cliente
And marco tipo de pessoa
And preencho o formulário de clientes com os seguintes campos:
|descricao|
|cliente do SIGFestas|
When clico em salvar
Then mensagem de erro deve aparecer de CPF obrigatorio
And clico em cancelar cadastro de cliente

Scenario: Cadastrar cliente sem descrição
Given estou na pagina de clientes
And clico no botão novo cliente
And marco tipo de pessoa
And preencho o formulário de clientes com os seguintes campos:
|cpf|
|107.423.836-45|
When clico em salvar
Then mensagem de erro deve aparecer de descricao obrigatoria
And clico em cancelar cadastro de cliente

Scenario: Consultar cliente existente
Given estou na pagina de clientes
When informo o valor 107.423.836-45 no campo de busca de tabela de cliente
Then exibe uma linha na tabela de clientes com cpf 107.423.836-45

Scenario: Consultar cliente inexistente 
Given estou na pagina de clientes
When informo o valor 107.423.836-45909090 no campo de busca de tabela de cliente
Then exibe nenhuma linha na tabela de cliente

