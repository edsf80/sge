package br.com.hyperactive.sge.test.pages;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;

public class ClientePage extends AbstractPage {

	public ClientePage(WebDriverProvider driverProvider) {
		super(driverProvider);
	}

	public void ehExibida() {
		$(By.xpath("html/body/div[2]/div/div/div/div[1]/div/h1")).shouldBe(visible);
	}

	public void clicarNovoCliente() {
		$(By.xpath("//*[@id='page-wrapper']/div/div[2]/div/div/p/button")).click();
		
	}

	public void preenchoFormularioCliente(String cpf, String descricao) {
		$(By.xpath("//*[@id='modal-body']/div[2]/input")).setValue(cpf);
		$(By.xpath("//*[@id='modal-body']/div[3]/input")).setValue(descricao);
	}
	
	public void marcoTipoCliente() {
		$(By.xpath("//*[@id='modal-body']/div[1]/label[2]")).click();
	}
	
	public void clicarSalvarCliente() {
		$(By.xpath("/html/body/div[1]/div/div/form/div[2]/input")).click();
	}
	
	public void sucessoCadastro() {
		$(By.xpath("/html/body/div[1]/div/div/div")).shouldBe(visible);

	}
	
	public boolean mensagemCampoCPFObrigatorioEhExibida() {
		return $(By.xpath("//*[@id='modal-body']/div[2]/div")).getText().equals("CPF é Requerido");	
		
	}

	public boolean mensagemCampoDescricaoObrigatorioEhExibida() {
		return $(By.xpath("//*[@id='modal-body']/div[3]/div")).getText().equals("Descrição é Requerida");	
	}

	public void clicarCancelarCadastro() {
		$(By.xpath("/html/body/div[1]/div/div/form/div[2]/button")).click();		
	}

	public void inserirBusca(String busca) {
		$(By.xpath("//*[@id='dtCliente_filter']/label/input")).setValue(busca);		
	}

	public boolean tabelaComResultado(String cpf) {
		return $(By.xpath("//*[@id='dtCliente']/tbody/tr[1]/td[2]")).getText().equals(cpf);
	}

	public boolean tabelaVazia(String string) {
		return $(By.xpath("//*[@id='dtCliente']/tbody/tr/td")).getText().equals(string);
	}

}
