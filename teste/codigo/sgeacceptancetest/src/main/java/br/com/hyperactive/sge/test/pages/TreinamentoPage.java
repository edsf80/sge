package br.com.hyperactive.sge.test.pages;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;

public class TreinamentoPage extends AbstractPage {

	public TreinamentoPage(WebDriverProvider driverProvider) {
		super(driverProvider);
	}

	public void ehExibida() {
		$(By.xpath("html/body/div[2]/div/div/div/div[1]/div/h1")).shouldBe(visible);
	}

	public void clicarNovoTreinamento() {
		$(By.xpath("html/body/div[2]/div/div/div/div[2]/div/div/p/button")).click();
	}

	public void clicarSalvarTreinamento() {
		$(By.xpath("/html/body/div[1]/div/div/form/div[2]/input")).click();
	}

	public void preenchoFormularioTreinamento(String titulo, String descricao) {
		$(By.xpath("//*[@id='modal-body']/div[1]/input")).setValue(titulo);
		$(By.xpath("//*[@id='modal-body']/div[2]/textarea")).setValue(descricao);
	}

	public void preenchoConteudo(String descricaoConteudo, String link) {
		$(By.xpath("//*[@id='modal-body']/div[3]/div[2]/div[1]/div/div/input")).setValue(descricaoConteudo);
		$(By.xpath("//*[@id='modal-body']/div[3]/div[2]/div[2]/div[1]/div/input")).setValue(link);
	}
	
	public void marcoTipoConteudo() {
		$(By.xpath("//*[@id='modal-body']/div[3]/div[2]/div[2]/div[2]/div/label[2]")).click();
	}
	
	public void clicoEmAdicionarConteudo() {
		$(By.xpath("//*[@id='modal-body']/div[3]/div[2]/div[3]/div/button")).click();
	}

	public void clicarCancelarTreinamento() {
		$(By.xpath("html/body/div[1]/div/div/form/div[2]/button")).click();
	}

	public void mensagemCampoTituloObrigatorioEhExibida() {
		$(By.xpath("html/body/div[1]/div/div/form/div[1]/div[1]/div")).shouldBe(visible);
	}

	public void mensagemCampoDescricaoObrigatorioEhExibida() {
		$(By.xpath("html/body/div[1]/div/div/form/div[1]/div[2]/div")).shouldBe(visible);
	}

	public void inserirBusca(String valor) {
		$(By.xpath("//*[@id='dtTreinamento_filter']/label/input")).setValue(valor);
	}

	public boolean tabelaComResultado(String titulo) {
		return $(By.xpath("//*[@id='dtTreinamento']/tbody/tr/td[2]")).getText().equals(titulo);
	}

	public void sucessoCadastro() {
		$(By.xpath("/html/body/div[1]/div/div/div")).shouldBe(visible);

	}

	public void clicarRemoverTreinamento() {
		$(By.xpath("//*[@id='dtTreinamento']/tbody/tr/td[1]/button[3]")).click();

	}

	public void clicarConfirmacaoRemocao() {
		$(By.xpath("/html/body/div[1]/div/div/div[3]/button[1]")).click();
	}

	public void clicarCancelarRemocao() {
		$(By.xpath("/html/body/div[1]/div/div/div[3]/button[2]")).click();
	}

	public boolean mensagemErroEmConteudo() {
		return $(By.xpath("//*[@id='modal-body']/div[4]")).getText()
				.equals("No mínimo um conteúdo deve ser adicionado");
	}

	public void clicarCancelarNovoTreinamento() {
		$(By.xpath("/html/body/div[1]/div/div/form/div[2]/button")).click();

	}

	public boolean mensagemErroEmTitulo() {
		return $(By.xpath("//*[@id='modal-body']/div[1]/div")).getText().equals("Título é Requerido");
	}

	public boolean mensagemErroEmDescricao() {
		return $(By.xpath("//*[@id='modal-body']/div[2]/div")).getText().equals("Descrição é Requerida");
	}

}
