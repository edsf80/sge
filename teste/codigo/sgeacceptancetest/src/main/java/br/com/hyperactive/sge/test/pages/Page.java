package br.com.hyperactive.sge.test.pages;

import org.jbehave.web.selenium.WebDriverProvider;

public class Page {
	private final WebDriverProvider driverProvider;
	//private final ConditionRunner conditionRunner;
	private LoginPage login;
	private HomePage home;
	private DemandaPage demanda;
	private TreinamentoPage treinamento;
	private ProvaPage prova;
	private ClientePage cliente;
	private NivelPage nivel;
	// More pages as needed

	public Page(WebDriverProvider driverProvider) {
		this.driverProvider = driverProvider;
	}

	public LoginPage login() {
		if (login == null) {
			login = new LoginPage(this.driverProvider);
		}
		return login;
	}

	public HomePage home() {
		if (home == null) {
			home = new HomePage(this.driverProvider);
		}
		return home;
	}
	
	public DemandaPage demanda() {
		if (demanda == null) {
			demanda = new DemandaPage(this.driverProvider);
		}
		return demanda;
	}
	
	public TreinamentoPage treinamento() {
		if (treinamento == null) {
			treinamento = new TreinamentoPage(this.driverProvider);
		}
		return treinamento;
	}
	
	public ProvaPage prova() {
		if(prova == null) {
			prova = new ProvaPage(this.driverProvider);
		}
		
		return prova;
	}
	
	public ClientePage cliente() {
		if(cliente == null) {
			cliente = new ClientePage(this.driverProvider);
		}
		
		return cliente;
	}
	
	public NivelPage nivel() {
		if(nivel == null) {
			nivel = new NivelPage(this.driverProvider);
		}
		
		return nivel;
	}
}
