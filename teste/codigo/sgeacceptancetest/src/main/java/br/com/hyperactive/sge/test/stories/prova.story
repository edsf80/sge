Story:
      Como Administrador 
      Eu desejo consultar, cadastrar ou atualizar provas
      para que os contribuintes possam ser avaliados após o treinamento

GivenStories: br/com/hyperactive/sge/test/stories/login.story#{id6:scenario6}

Scenario: Consultar provas 
Given Eu estou na tela principal
When Clico no menu provas
Then Vai para tela de consulta de provas

Scenario: Consultar prova inexistente
Given Eu estou na tela de consulta de provas
When Informar o valor xxx no campo de busca
Then Não será exibido nenhum valor na tabela de prova

Scenario: Inserir nova prova falha campos obrigatórios
Given Eu estou na tela de consulta de provas
When Clico no botão Nova Prova
And Clico no botão salvar prova
Then Mensagem de erro no campo obrigatório assunto da prova é exibida
Then Mensagem de erro nenhuma prova adicionada
And Clico no botão cancelar prova

Scenario: Tentar inserir uma questão invalida em uma prova
Given Eu estou na tela de consulta de provas
When Clico no botão Nova Prova
Then Estou na tela de nova Prova
When Clico no botão abrir questão
And Clico no botão Adicionar Questão
Then Mensagem de erro no campo obrigatório enunciado da questao é exibida
Then Mensagem de erro no campo obrigatório alternativa da questao é exibida
And Clico no botão cancelar questao
And Clico no botão cancelar prova
Then Vai para tela de consulta de provas


