Story:
      Como Administrador 
      Eu desejo consultar, cadastrar ou atualizar demandas
      para registrar e ter histórico das demandas da empresa

GivenStories: br/com/hyperactive/sge/test/stories/login.story#{id6:scenario6}

Scenario: Consultar demandas 
Given Eu estou na tela principal
When Clicar no menu demandas
Then Vai para tela de consulta de demandas

Scenario: Consultar demanda específica
Given Eu estou na tela de consulta de demandas
When Informar o valor demanda no campo de busca
Then Será exibida apenas a linha com a descrição Nova demanda  

Scenario: Consultar demanda inexistente
Given Eu estou na tela de consulta de demandas
When Informar o valor xxx no campo de busca
Then Não será exibido nenhum valor na tabela de demanda

Scenario: Inserir nova demanda falha campos obrigatórios
Given Eu estou na tela de consulta de demandas
When Clico no botão Nova Demanda
And Clico no botão salvar demanda
Then Mensagem de erro no campo obrigatório descrição da demanda é exbida
And Mensagem de erro no campo obrigatório status da demanda é exbida
And Clico no botão cancelar demanda