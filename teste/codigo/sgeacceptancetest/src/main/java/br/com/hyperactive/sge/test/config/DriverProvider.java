/**
 * 
 */
package br.com.hyperactive.sge.test.config;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.codeborne.selenide.WebDriverRunner;

/**
 * @author edsf
 *
 */
public class DriverProvider implements WebDriverProvider {

	private WebDriver driver;
	/* (non-Javadoc)
	 * @see org.jbehave.web.selenium.WebDriverProvider#get()
	 */
	public WebDriver get() {
		// TODO Auto-generated method stub
		return driver;
	}

	/* (non-Javadoc)
	 * @see org.jbehave.web.selenium.WebDriverProvider#initialize()
	 */
	public void initialize() {
		System.setProperty("webdriver.chrome.driver",
				"/home/millanium/Projects/HyperactiveJR/sge/teste/codigo/sgeacceptancetest/lib/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-extensions");
		
		driver = new ChromeDriver(options);
		
		driver.manage().window().maximize();
		System.setProperty("selenide.browser", "Chrome");
		WebDriverRunner.setWebDriver(driver);

	}

	/* (non-Javadoc)
	 * @see org.jbehave.web.selenium.WebDriverProvider#saveScreenshotTo(java.lang.String)
	 */
	public boolean saveScreenshotTo(String path) {
		File screen = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screen, new File(path));
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see org.jbehave.web.selenium.WebDriverProvider#end()
	 */
	public void end() {
		driver.close();
		driver.quit();

	}

}
