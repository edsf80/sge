Story:
	Como administrador
	Eu gostaria de cadastrar, atualizar e remover treinamentos
	Para conseguir manter o histórico de todos os treinamentos oferecidos pela empresa

Scenario: Abrir pagina treinamentos
Given eu estou na tela principal
When clico no menu treinamentos
Then vai para tela de consulta de treinamentos


Scenario: Cadastrar treinamento com sucesso
Given estou na pagina de treinamento
And clico no no botão novo treinamento
And preencho o formulário com os seguintes campos:
|titulo|descricao|
|Spring boot|Curso Spring Boot|
And preencho o formulário de conteudo com os seguintes campos:
|descricaoConteudo|link|
|Spring boot|spring.io|
And marco tipo de conteudo
And clico em adicionar conteudo
When clico em salvar
Then mensagem de sucesso deve aparecer

Scenario: Cadastrar treinamento sem conteudo 
Given estou na pagina de treinamento
And clico no no botão novo treinamento
And preencho o formulário com os seguintes campos:
|titulo|descricao|
|Spring boot|Curso Spring Boot|
When clico em salvar
Then mensagem de erro deve aparecer de conteudo obrigatario
And clico em cancelar cadastro de treianamento

Scenario: Cadastrar treinamento sem descricao de conteudo
Given estou na pagina de treinamento
And clico no no botão novo treinamento
And preencho o formulário com os seguintes campos:
|titulo|descricao|
|Spring boot|Curso Spring Boot|
And preencho o formulário de conteudo com os seguintes campos:
|link|
|spring.io|
And marco tipo de conteudo
And clico em adicionar conteudo
When clico em salvar
Then mensagem de erro deve aparecer de conteudo obrigatario
And clico em cancelar cadastro de treianamento

Scenario: Cadastrar treinamento sem link de conteudo
Given estou na pagina de treinamento
And clico no no botão novo treinamento
And preencho o formulário com os seguintes campos:
|titulo|descricao|
|Spring boot|Curso Spring Boot|
And preencho o formulário de conteudo com os seguintes campos:
|descricaoConteudo|
|Spring boot|
And marco tipo de conteudo
And clico em adicionar conteudo
When clico em salvar
Then mensagem de erro deve aparecer de conteudo obrigatario
And clico em cancelar cadastro de treianamento

Scenario: Cadastrar treinamento sem marcar tipo de conteudo
Given estou na pagina de treinamento
And clico no no botão novo treinamento
And preencho o formulário com os seguintes campos:
|titulo|descricao|
|Spring boot|Curso Spring Boot|
And preencho o formulário de conteudo com os seguintes campos:
|descricaoConteudo|link|
|Spring boot|spring.io|
And clico em adicionar conteudo
When clico em salvar
Then mensagem de erro deve aparecer de conteudo obrigatario
And clico em cancelar cadastro de treianamento

Scenario: Cadastrar treinamento sem titulo 
Given estou na pagina de treinamento
And clico no no botão novo treinamento
And preencho o formulário com os seguintes campos:
|descricao|
|Curso Spring Boot|
When clico em salvar
Then mensagem de erro deve aparecer de titulo obrigatorio
And clico em cancelar cadastro de treianamento

Scenario: Cadastrar treinamento sem descricao 
Given estou na pagina de treinamento
And clico no no botão novo treinamento
And preencho o formulário com os seguintes campos:
|titulo|
|Curso Spring Boot|
When clico em salvar
Then mensagem de erro deve aparecer de descricao obrigatorio
And clico em cancelar cadastro de treianamento

Scenario: Consultar treinamento existente
Given estou na pagina de treinamento
When informo o valor Spring boot no campo de busca
Then exibe uma linha na tabela de treinamentos com titulo Spring boot

Scenario: Remover Treinamento
Given estou na pagina de treinamento
And clico no botao de remover de um item
And confirmo remocao
Then mensagem de sucesso deve aparecer

Scenario: Remover Treinamento e desistir
Given estou na pagina de treinamento
And clico no botao de remover de um item
And cancelo remocao
Then mensagem de sucesso deve aparecer