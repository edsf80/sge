package br.com.hyperactive.sge.test.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import br.com.hyperactive.sge.test.pages.Page;

public class LoginSteps {
	
	private Page page;
	
	public LoginSteps(Page page) {
		this.page = page;
	}

	@Given("Eu estou na tela de login")
	public void irParaLogin() {
		if(!page.home().estaDisponivel()) {
			page.login().abrir();
		} else {
			page.home().clicarLogout();
			page.login().abrir();
		}
	}
	
	@Given("Eu efetuo logout no sistema")
	public void efetuarLogout() {
		page.home().clicarLogout();
	}
	
	@When("Inserir login $login e senha $senha")
	public void inserirLoginESenha(String login, String senha) {
		if(!login.equals("vazio"))
			page.login().inserirLogin(login);
		if(!senha.equals("vazio"))
			page.login().inserirSenha(senha);		
	}
	
	@When("clicar no botão login")
	public void clicarLogin() {
		page.login().clicarLogin();
	}
	
	@Then("Mensagem de falha de validação deve aparecer") 
	public void mostrarFalhaDeValidacao() {
		page.login().falhaDeValidacao();		
	}
	
	@Then("Mensagem de campo login não preenchido deve aparecer") 
	public void mostrarFalhaCampoObrigatorioLogin() {
		page.login().falhaCampoLoginVazio();
	}
	
	@Then("Mensagem de campo senha não preenchido deve aparecer") 
	public void mostrarFalhaCampoObrigatorioSenha() {
		page.login().falhaCampoSenhaVazio();
	}
	
	@Then("Vai para tela principal do sistema") 
	public void irParaPaginaDeLogin() {
		page.home().ehExibida();		
	}
}
