package br.com.hyperactive.sge.test.steps;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import br.com.hyperactive.sge.test.pages.Page;

public class DemandaSteps {
	
	private Page page;
	
	public DemandaSteps(Page page) {
		this.page = page;
	}
	
	@Given("Eu estou na tela principal")
	public void irParaLogin() {
		page.home().abrir();
	}
	
	
	@When("Clicar no menu demandas")
	public void clicarNoMenuDemandas() {
		page.home().clicarMenuDemandas();
	}
	
	@Given("Eu estou na tela de consulta de demandas")
	@Then("Vai para tela de consulta de demandas")
	public void irParaTelaDeDemandas() {
		page.demanda().ehExibida();
	}
	
	@When("Informar o valor $busca no campo de busca") 
	public void inserirBusca(String busca) {
		page.demanda().inserirBusca(busca);
	}
	
	@When("Clico no botão Nova Demanda") 
	public void clicarBotaoNovaDemanda() {
		page.demanda().clicarNovaDemanda();
	}
	
	@When("Clico no botão salvar demanda") 
	public void clicarBotaoSalvarDemanda() {
		page.demanda().clicarSalvarDemanda();
	}
	
	@Then("Será exibida apenas a linha com a descrição $busca") 
	public void ehExibidoResultado(String busca) {
		assertTrue(page.demanda().ehExibidaDescricaoBusca(busca));
	}
	
	@Then("Não será exibido nenhum valor na tabela de demanda") 
	public void nadaExibidoNaTabela() {
		assertTrue(page.demanda().tabelaEstaVazia());
	}
	
	@Then("Mensagem de erro no campo obrigatório descrição da demanda é exbida") 
	public void msgErroCampoDescricaoEhExibida() {
		page.demanda().mensagemCampoDescricaoObrigatoriaEhExibida();
	}
	
	@Then("Mensagem de erro no campo obrigatório status da demanda é exbida") 
	public void msgErroCampoStatusEhExibida() {
		page.demanda().mensagemCampoStatusObrigatorioEhExibida();
	}
	
	@Then("Clico no botão cancelar demanda") 
	public void clicoCancelarDemanda() {
		page.demanda().clicarCancelarDemanda();
	}

}
