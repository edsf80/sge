package br.com.hyperactive.sge.test.steps;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

import br.com.hyperactive.sge.test.pages.Page;

public class NivelSteps {

	private Page page;

	public NivelSteps(Page page) {
		this.page = page;
	}

	@Given("eu estou na tela principal")
	public void irParaLogin() {
		page.home().abrir();
	}

	@When("clico no menu niveis")
	public void clicarNoMenuClientes() {
		page.home().clicarMenuNiveis();
	}

	@Given("estou na pagina de clientes")
	@Then("vai para tela de consulta de clientes")
	public void irParaTelaDeDemandas() {
		page.nivel().ehExibida();
	}
}
