package br.com.hyperactive.sge.test.pages;

import static com.codeborne.selenide.Condition.appears;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;

public class HomePage extends AbstractPage {

	public HomePage(WebDriverProvider driverProvider) {
		super(driverProvider);
	}
	
	public void abrir() {
		open("http://localhost:8080/sge/#/home");
	}
	
	public boolean estaDisponivel() {		
		return $(".navbar-brand").isDisplayed();
	}
	
	public void ehExibida() {
		$(".navbar-brand").shouldBe(visible);
	}
	
	public void clicarMenuDemandas() {
		$(By.xpath("html/body/div[2]/div/nav/div[2]/div/ul/li[1]/a")).should(appears);
		$(By.xpath("html/body/div[2]/div/nav/div[2]/div/ul/li[1]/a")).click();
	}
	public void clicarMenuTreinamentos() {
		$(By.xpath("html/body/div[2]/div/nav/div[2]/div/ul/li[4]/a")).should(appears);
		$(By.xpath("html/body/div[2]/div/nav/div[2]/div/ul/li[4]/a")).click();
	}
	
	public void clicarMenuClientes() {
		$(By.xpath("//*[@id='side-menu']/li[3]/a")).should(appears);
		$(By.xpath("//*[@id='side-menu']/li[3]/a")).click();
	}
	
	public void clicarMenuNiveis() {
		$(By.xpath("//*[@id='side-menu']/li[4]/a")).should(appears);
		$(By.xpath("//*[@id='side-menu']/li[4]/a")).click();
	}
	
	public void clicarMenuProvas() {
		$(By.xpath("html/body/div[2]/div/nav/div[2]/div/ul/li[5]/a")).should(appears);
		$(By.xpath("html/body/div[2]/div/nav/div[2]/div/ul/li[5]/a")).click();
	}
	
	public void clicarLogout() {
		$(By.xpath("html/body/div[2]/div/nav/ul/li/a/i[2]")).should(appears);
		$(By.xpath("html/body/div[2]/div/nav/ul/li/a/i[2]")).click();		
		$(By.xpath("html/body/div[2]/div/nav/ul/li/ul/li[4]/a")).should(appears);
		$(By.xpath("html/body/div[2]/div/nav/ul/li/ul/li[4]/a")).click();
	}
}
