Story:
   Como usuario 
   Eu desejo efetuar o login no sistema
   para utiliza-lo

Scenario: Login com nome de usuário inválido
Meta: @id1
Given Eu estou na tela de login
When Inserir login xxxxxx e senha 123
And clicar no botão login
Then Mensagem de falha de validação deve aparecer

Scenario: Login com senha inválida
Meta: @id2
Given Eu estou na tela de login
When Inserir login 03481656416 e senha xxxxxx
And clicar no botão login
Then Mensagem de falha de validação deve aparecer

Scenario: Login com senha não preenchida
Meta: @id3
Given Eu estou na tela de login
When Inserir login 03481656416 e senha vazio 
And clicar no botão login
Then Mensagem de campo senha não preenchido deve aparecer

Scenario: Login nome de usuário não preenchido
Meta: @id4
Given Eu estou na tela de login
When Inserir login vazio e senha 123
And clicar no botão login
Then Mensagem de campo login não preenchido deve aparecer

Scenario: Login nome de usuário e senha não preenchida
Meta: @id5
Given Eu estou na tela de login
When Inserir login vazio e senha vazio
And clicar no botão login
Then Mensagem de campo login não preenchido deve aparecer
And Mensagem de campo senha não preenchido deve aparecer

Scenario: Login com Sucesso
Meta: @id6 scenario6
Given Eu estou na tela de login
When Inserir login 03481656416 e senha 123
And clicar no botão login
Then Vai para tela principal do sistema