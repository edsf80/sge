package br.com.hyperactive.sge.test.steps;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

import br.com.hyperactive.sge.test.pages.Page;

public class ClienteSteps {

	private Page page;

	public ClienteSteps(Page page) {
		this.page = page;
	}

	@Given("eu estou na tela principal")
	public void irParaLogin() {
		page.home().abrir();
	}

	@When("clico no menu clientes")
	public void clicarNoMenuClientes() {
		page.home().clicarMenuClientes();
	}

	@Given("estou na pagina de clientes")
	@Then("vai para tela de consulta de clientes")
	public void irParaTelaDeDemandas() {
		page.cliente().ehExibida();
	}
	
	@Given("clico no botão novo cliente")
	public void clicarBotaoNovoCliente() {
		page.cliente().clicarNovoCliente();
	}
	
	@Given("marco tipo de pessoa")
	public void marcoTipoConteudo() {
		
		page.cliente().marcoTipoCliente();
	}
	
	@Given("preencho o formulário de clientes com os seguintes campos: $table")
	public void armazenarCampos(ExamplesTable table) {
		Map<String, String> row = table.getRow(0);
		String cpf = row.get("cpf");
		String descricao = row.get("descricao");
		page.cliente().preenchoFormularioCliente(cpf, descricao);

	}
	
	@When("clico em salvar")
	public void clicarBotaoSalvarCliente() {
		page.cliente().clicarSalvarCliente();
	}
	
	@Then("mensagem de sucesso deve aparecer")
	public void mensagemSucessoSalvar() {
		page.cliente().sucessoCadastro();
	}
	
	@Then("mensagem de erro deve aparecer de CPF obrigatorio")
	public void mensagemErroEmCPF() {
		assertTrue(page.cliente().mensagemCampoCPFObrigatorioEhExibida());
	}
	
	@Then("mensagem de erro deve aparecer de descricao obrigatoria")
	public void mensagemErroEmDescricao() {
		assertTrue(page.cliente().mensagemCampoDescricaoObrigatorioEhExibida());
	}
	
	@Then("clico em cancelar cadastro de cliente")
	public void clicarBotaoCancelarCadastroCliente() {
		page.cliente().clicarCancelarCadastro();
	}
	
	@When("informo o valor $busca no campo de busca de tabela de cliente")
	public void inserirBusca(String busca) {
		page.cliente().inserirBusca(busca);
	}
	
	@Then("exibe uma linha na tabela de clientes com cpf $cpf")
	public void exibeResultadoNaTabela(String cpf) {
		assertTrue(page.cliente().tabelaComResultado(cpf));
	}
	
	@Then("exibe nenhuma linha na tabela de cliente")
	public void exibeNenhumResultadoNaTabela() {
		assertTrue(page.cliente().tabelaVazia("Nenhum item encontrado no filtro"));
	}
	

}
