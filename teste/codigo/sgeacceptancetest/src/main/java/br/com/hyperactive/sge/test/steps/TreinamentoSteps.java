package br.com.hyperactive.sge.test.steps;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

import br.com.hyperactive.sge.test.pages.Page;

public class TreinamentoSteps {

	private Page page;

	public TreinamentoSteps(Page page) {
		this.page = page;
	}

	@Given("eu estou na tela principal")
	public void irParaLogin() {
		page.home().abrir();
	}

	@When("clico no menu treinamentos")
	public void clicarNoMenuDemandas() {
		page.home().clicarMenuTreinamentos();
	}

	@Given("estou na pagina de treinamento")
	@Then("vai para tela de consulta de treinamentos")
	public void irParaTelaDeDemandas() {
		page.treinamento().ehExibida();
	}

	@Given("clico no no botão novo treinamento")
	public void clicarBotaoNovoTreinamento() {
		page.treinamento().clicarNovoTreinamento();
	}

	@Given("preencho o formulário com os seguintes campos: $table")
	public void armazenarCampos(ExamplesTable table) {
		Map<String, String> row = table.getRow(0);
		String titulo = row.get("titulo");
		String descricao = row.get("descricao");
		
		page.treinamento().preenchoFormularioTreinamento(titulo, descricao);

	}

	@Given("preencho o formulário de conteudo com os seguintes campos: $table")
	public void preenchoCamposConteudo(ExamplesTable table) {
		Map<String, String> rowConteudo = table.getRow(0);
		String descricaoConteudo = rowConteudo.get("descricaoConteudo");
		String link = rowConteudo.get("link");
		
		page.treinamento().preenchoConteudo(descricaoConteudo, link);
	}
	
	@Given("marco tipo de conteudo")
	public void marcoTipoConteudo() {

		page.treinamento().marcoTipoConteudo();
	}
	
	@Given("clico em adicionar conteudo")
	public void clicoAdicionarConteudo() {

		page.treinamento().clicoEmAdicionarConteudo();
	}
	

	@When("clico em salvar")
	public void clicarBotaoSalvarTreinamento() {
		page.treinamento().clicarSalvarTreinamento();
	}
	
	@Then("mensagem de sucesso deve aparecer")
	public void mensagemSucessoSalvar() {
		page.treinamento().sucessoCadastro();
	}

	@When("informo o valor $busca no campo de busca")
	public void inserirBusca(String busca) {
		page.treinamento().inserirBusca(busca);
	}

	@Then("exibe uma linha na tabela de treinamentos com titulo $titulo")
	public void exibeResultadoNaTabela(String titulo) {
		assertTrue(page.treinamento().tabelaComResultado(titulo));
	}
	
	@Given("clico no botao de remover de um item")
	public void clicarBotaoRemoverTreinamento() {
		page.treinamento().clicarRemoverTreinamento();
	}
	
	@Given("confirmo remocao")
	public void clicarBotaoConfirmarRemocaoTreinamento() {
		page.treinamento().clicarConfirmacaoRemocao();
	}
	
	@Given("cancelo remocao")
	public void clicarBotaoCancelarRemocaoTreinamento() {
		page.treinamento().clicarCancelarRemocao();
	}
	
	@Then("mensagem de erro deve aparecer de conteudo obrigatario")
	public void mensagemErroEmConteudo() {
		assertTrue(page.treinamento().mensagemErroEmConteudo());
	}
	
	@Then("mensagem de erro deve aparecer de titulo obrigatorio")
	public void mensagemErroEmTitulo() {
		assertTrue(page.treinamento().mensagemErroEmTitulo());
	}
	
	@Then("mensagem de erro deve aparecer de descricao obrigatorio")
	public void mensagemErroEmDescricao() {
		assertTrue(page.treinamento().mensagemErroEmDescricao());
	}
	
	@Then("clico em cancelar cadastro de treianamento")
	public void clicarBotaoCancelarCadastroTreinamento() {
		page.treinamento().clicarCancelarNovoTreinamento();
	}
}
