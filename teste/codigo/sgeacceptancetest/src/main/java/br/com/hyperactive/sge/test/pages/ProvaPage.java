/**
 * 
 */
package br.com.hyperactive.sge.test.pages;

import static com.codeborne.selenide.Condition.appears;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;

/**
 * @author edsf
 *
 */
public class ProvaPage extends AbstractPage {

	public ProvaPage(WebDriverProvider driverProvider) {
		super(driverProvider);
	}

	public void ehExibida() {
		$(By.xpath("html/body/div[2]/div/div/div/div[1]/div/h1")).shouldBe(visible);
	}

	public void inserirBusca(String valor) {
		$(By.xpath("html/body/div[2]/div/div/div/div[2]/div/div/div/div[1]/div[2]/div/label/input")).setValue(valor);
	}

	public boolean ehExibidaDescricaoBusca(String busca) {
		return $(By.xpath("html/body/div[2]/div/div/div/div[2]/div/div/div/table/tbody/tr/td[2]")).getText().equals(busca);
	}

	public boolean tabelaEstaVazia() {		
		return $(By.xpath("html/body/div[2]/div/div/div/div[2]/div/div/div/table/tbody/tr/td")).getText().equals("Nenhum item encontrado no filtro");
	}
	
	public void clicarNovaProva() {
		$(By.xpath("html/body/div[2]/div/div/div/div[2]/div/div/p/a")).click();
	}
	
	public void clicarSalvarProva() {
		$(By.xpath("html/body/div[2]/div/div/div/div/form/input")).click();		
	}
	
	public void mensagemCampoDescricaoObrigatoriaEhExibida() {
		$(By.xpath("html/body/div[2]/div/div/div/div/form/div[1]/div")).shouldBe(visible);		
	}
	
	public boolean mensagemCampoProvaObrigatoriaEhExibida() {
		return $(By.xpath("html/body/div[2]/div/div/div/div/form/help-block")).getText().equals("Uma pergunta deve ser adicionada.");
	}
	
	public void clicarCancelarProva() {
		$(By.xpath("html/body/div[2]/div/div/div/div/form/button")).click();		
	}
	
	public void clicarAbrirQuestao() {
		$(By.xpath("html/body/div[2]/div/div/div/div/form/div[2]/div/div[2]/button")).click();		
	}
	
	public void clicarAdicionarQuestao() {
		$(By.xpath("html/body/div[1]/div/div/form/div[2]/input")).click();		
	}
	
	public void mensagemCampoEnunciadoObrigatoriaEhExibida() {
		$(By.xpath("html/body/div[1]/div/div/form/div[1]/div[1]/div")).shouldBe(visible);		
	}
	
	public boolean mensagemCampoAlternativaObrigatoriaEhExibida() {
		return $(By.xpath("html/body/div[1]/div/div/form/div[1]/div[2]/help-block")).getText().equals("Uma alternativa deve ser marcada como correta.");	
	}
	
	public void clicarCancelarQuestao() {
		$(By.xpath("html/body/div[1]/div/div/form/div[2]/button")).click();		
	}
	
	public void inserirEnunciado(String enunciado) {
		$(By.xpath("//*[@id='tEnunciado']")).setValue(enunciado);
	}
	
	public boolean clicoAlternativa() {
		$(By.xpath("//*[@id='modal-body']/div[2]/div[1]/button")).click();
		return $(By.xpath("//*[@id='modal-body']/div[2]/div[1]/button")).getText().equals("CORRETA");
	}
	
	public void adicionoAlternativa(String alternativa) {
		for (int i = 1; i <= 5; i++) {
			$(By.xpath("//*[@id='modal-body']/div[2]/div["+i+"]/a")).click();
			$(By.xpath("//*[@id='modal-body']/div[2]/div["+i+"]/div/textarea")).setValue(alternativa);
		}
	}
	
	public void selecionoNivelQuestao() {
		$(By.xpath("//*[@id='modal-body']/div[3]/select")).selectOptionByValue("FACIL");
	}
	
	public void provaAdicionadaSucesso() {
		$(By.xpath("//*[@id='page-wrapper']/div/div/form/div[4]/table")).shouldBe(visible);
	}
	
	public void ehExibidaTelaNovaProva() {
		$(By.xpath("//*[@id='page-wrapper']/div/div/div/h1")).shouldBe(visible);
	}
	
	public void ehExibidaTelaNovaQuestao() {
		$(By.xpath("/html/body/div[1]/div/div/header-modal/div/h4")).shouldBe(visible);
	}
	
	public void inserirAssunto(String assunto) {
		$("#tAssunto").should(appears);
		$("#tLogin").setValue(assunto);
	}

}
