package br.com.hyperactive.sge.test.steps;

import static org.junit.Assert.assertTrue;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import br.com.hyperactive.sge.test.pages.Page;

public class ProvaSteps {
	
	private Page page;
	
	public ProvaSteps(Page page) {
		this.page = page;
	}
	
	@Given("Eu estou na tela principal")
	public void irParaLogin() {
		page.home().abrir();
	}
	
	
	@When("Clico no menu provas")
	public void clicarNoMenuProvas() {
		page.home().clicarMenuProvas();
	}
	
	@Given("Eu estou na tela de consulta de provas")
	@Then("Vai para tela de consulta de provas")
	public void irParaTelaDeProvas() {
		page.prova().ehExibida();
	}
	
	@When("Informar o valor $busca no campo de busca") 
	public void inserirBusca(String busca) {
		page.prova().inserirBusca(busca);
	}
	
	@Then("Não será exibido nenhum valor na tabela de prova") 
	public void nadaExibidoNaTabela() {
		assertTrue(page.prova().tabelaEstaVazia());
	}
	
	
	@When("Clico no botão Nova Prova") 
	public void clicarBotaoNovaProva() {
		page.prova().clicarNovaProva();
	}
	
	@When("Clico no botão salvar prova") 
	public void clicarBotaoSalvarProva() {
		page.prova().clicarSalvarProva();
	}

	
	@Then("Mensagem de erro no campo obrigatório assunto da prova é exibida") 
	public void msgErroCampoAssuntoEhExibida() {
		page.prova().mensagemCampoDescricaoObrigatoriaEhExibida();
	}
	
	@Then("Mensagem de erro nenhuma prova adicionada") 
	public void msgErroNengumaProvaAdicionadaEhExibida() {
		assertTrue(page.prova().mensagemCampoProvaObrigatoriaEhExibida());
	}

	
	@Then("Clico no botão cancelar prova") 
	public void clicoCancelarProva() {
		page.prova().clicarCancelarProva();
	}
	
	@When("Clico no botão abrir questão") 
	public void clicoAbrirQuestao() {
		page.prova().clicarAbrirQuestao();
	}
	
	@When("Clico no botão Adicionar Questão") 
	public void clicarSalavarQuestao() {
		page.prova().clicarAdicionarQuestao();
	}
	
	@Then("Mensagem de erro no campo obrigatório enunciado da questao é exibida") 
	public void msgErroCampoEnunciadoEhExibida() {
		page.prova().mensagemCampoEnunciadoObrigatoriaEhExibida();
	}
	
	@Then("Mensagem de erro no campo obrigatório alternativa da questao é exibida") 
	public void msgErroCampoAlternativaEhExibida() {
		page.prova().mensagemCampoAlternativaObrigatoriaEhExibida();
	}
	
	@Then("Clico no botão cancelar questao") 
	public void clicoCancelarQuestao() {
		page.prova().clicarCancelarQuestao();
	}
	
	@When("Inserir enunciado $enunciado")
	public void inserirEnunciado(String enunciado) {
		page.prova().inserirEnunciado(enunciado);
	}
	
	@When("marco uma alternativa")
	public void marcoAlternativa() {
		page.prova().clicoAlternativa();
	}
	
	@When("adiciono texto em alternativa $alternativa")
	public void adicionoAlternativa(String alternativa) {
		page.prova().adicionoAlternativa(alternativa);
	}
	
	@When("seleciono um nivel")
	public void selecionoNivelQuestao() {
		page.prova().selecionoNivelQuestao();
	}
	
	@When("prova deve ter sido adicionada")
	public void provaAdicionadaSucesso() {
		page.prova().provaAdicionadaSucesso();
	}
	
	@Given("Eu estou na tela de nova prova")
	@Then("Estou na tela de nova Prova") 
	public void irParaPaginaDeProva() {
		page.prova().ehExibidaTelaNovaProva();	
	}
	
	@Then("Pagina de adicionar Questao deve aparecer") 
	public void irParaPaginaDeQuestao() {
		page.prova().ehExibidaTelaNovaQuestao();	
	}
	
	@When("adiciono assunto $assunto")
	public void inserirAssunto(String assunto) {
		page.prova().inserirAssunto(assunto);
	}

}
