/**
 * 
 */
package br.com.hyperactive.sge.service.negocio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.hyperactive.sge.data.dao.AlunoDao;
import br.com.hyperactive.sge.domain.entity.Aluno;

/**
 * @author edsf
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private AlunoDao alunoDao;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		
	

		Aluno aluno = alunoDao.findByMatricula(login);
		UserDetailsImpl userDetails = new UserDetailsImpl();
		userDetails.setNome(aluno.getNome());
		userDetails.setUsername(aluno.getMatricula());
		userDetails.setPassword("edsf");
		
		return userDetails;
	}

}
