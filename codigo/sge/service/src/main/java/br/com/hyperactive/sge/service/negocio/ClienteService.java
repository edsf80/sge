package br.com.hyperactive.sge.service.negocio;

import java.util.List;

import br.com.hyperactive.sge.domain.entity.Cliente;

public interface ClienteService {
	
	List<Cliente> buscarTodos();
	
	Cliente salvar(Cliente cliente);

}
