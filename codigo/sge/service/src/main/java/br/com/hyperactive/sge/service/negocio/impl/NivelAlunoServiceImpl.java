/**
 * 
 */
package br.com.hyperactive.sge.service.negocio.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hyperactive.sge.data.dao.NivelAlunoDao;
import br.com.hyperactive.sge.domain.entity.NivelAluno;
import br.com.hyperactive.sge.service.negocio.NivelAlunoService;

/**
 * @author edsf
 *
 */
@Service
public class NivelAlunoServiceImpl implements NivelAlunoService {

	@Autowired
	private NivelAlunoDao nivelAlunoDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.hyperactive.sge.service.negocio.NivelAlunoService#salvar(br.com.
	 * hyperactive.sge.domain.entity.NivelAluno)
	 */
	@Transactional
	@Override
	public NivelAluno salvar(NivelAluno nivelAluno) {

		return nivelAlunoDao.save(nivelAluno);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.hyperactive.sge.service.negocio.NivelAlunoService#apagar(java.lang
	 * .Long)
	 */
	@Transactional
	@Override
	public void apagar(Long id) {
		nivelAlunoDao.delete(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.hyperactive.sge.service.negocio.NivelAlunoService#
	 * buscarPorMatricula(java.lang.String)
	 */
	public List<NivelAluno> buscarPorMatricula(String matricula) {
		return nivelAlunoDao.findByMatricula(matricula);
	}

	/**
	 * 
	 */
	@Override
	public List<NivelAluno> buscarPorPrazo(Date inecio, Date fim) {
		return nivelAlunoDao.findByPrazoConclusaoBetween(inecio, fim);
	}

	/**
	 * 
	 */
	@Override
	public List<NivelAluno> buscarTodos() {
		return nivelAlunoDao.findAll();
	}

}
