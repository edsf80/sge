/**
 * 
 */
package br.com.hyperactive.sge.service.negocio.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hyperactive.sge.data.dao.AdmissaoDao;
import br.com.hyperactive.sge.domain.entity.Admissao;
import br.com.hyperactive.sge.service.negocio.AdmissaoService;

/**
 * @author edsf
 *
 */
@Service
public class AdmissaoServiceImpl implements AdmissaoService {

	@Autowired
	private AdmissaoDao admissaoDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.hyperactive.sge.service.negocio.AdmissaoService#criar(br.com.
	 * hyperactive.sge.domain.entity.Admissao)
	 */
	@Override
	@Transactional
	public Admissao criar(Admissao admissao) {
		return admissaoDao.save(admissao);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.hyperactive.sge.service.negocio.AdmissaoService#alterar(br.com.
	 * hyperactive.sge.domain.entity.Admissao)
	 */
	@Override
	@Transactional
	public Admissao alterar(Admissao admissao) {
		return admissaoDao.save(admissao);
	}

}
