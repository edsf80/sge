package br.com.hyperactive.sge.service.negocio;

import java.util.List;

import br.com.hyperactive.sge.domain.entity.Nivel;
import br.com.hyperactive.sge.domain.entity.Treinamento;

/**
 * 
 * @author André
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */

public interface TreinamentoService {
	
	Treinamento salvar(Treinamento treinamento);
	
	Treinamento alterar(Treinamento treinamento);
	
	Treinamento buscarPorId(Long id);
	
	List<Treinamento> buscarPorNivel(Nivel nivel);
	
	List<Treinamento> buscarTodos();
	
	void remover(Treinamento treinamento);
	
}
