/**
 * 
 */
package br.com.hyperactive.sge.service.negocio;

import java.util.List;

import br.com.hyperactive.sge.domain.entity.Aluno;

/**
 * @author edsf
 *
 */
public interface AlunoService {

	List<Aluno> buscarTodos();
	
	Aluno buscarPorMatricula(String matricula);
}
