/**
 * 
 */
package br.com.hyperactive.sge.service.negocio;

import br.com.hyperactive.sge.domain.entity.Aluno;

/**
 * @author edsf
 *
 */
public interface LoginService {

	Aluno efetuarLogin(String login, String senha);
	
	Aluno buscarUsuarioLogado();
}
