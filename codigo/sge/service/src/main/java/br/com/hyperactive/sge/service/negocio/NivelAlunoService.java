/**
 * 
 */
package br.com.hyperactive.sge.service.negocio;

import java.util.Date;
import java.util.List;

import br.com.hyperactive.sge.domain.entity.NivelAluno;

/**
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
public interface NivelAlunoService {

	NivelAluno salvar(NivelAluno nivelAluno);
	
	void apagar(Long id);
	
	List<NivelAluno> buscarPorMatricula(String matricula);
	
	List<NivelAluno> buscarPorPrazo(Date inicio, Date fim);
	
	List<NivelAluno> buscarTodos();
}
