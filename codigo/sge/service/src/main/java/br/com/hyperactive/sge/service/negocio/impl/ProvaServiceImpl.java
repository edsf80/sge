package br.com.hyperactive.sge.service.negocio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hyperactive.sge.data.dao.ProvaDao;
import br.com.hyperactive.sge.domain.entity.Prova;
import br.com.hyperactive.sge.service.negocio.ProvaService;

/**
 * 
 * <p>
 * <b>Implementação de serviços de uma @Prova</b>
 * </p>
 *
 * <p>
 * 	Implementa todos os métodos definidos por @ProvaService.
 * </p>
 *
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>	
 *
 */
@Service
public class ProvaServiceImpl implements ProvaService{
	
	@Autowired
	private ProvaDao provaDao;

	/**
	 * 
	 */
	@Override
	@Transactional
	public Prova salvar(Prova prova) {
		return provaDao.save(prova);
	}

	/**
	 * 
	 */
	@Override
	@Transactional
	public Prova alterar(Prova prova) {
		return provaDao.save(prova);
	}

	/**
	 * 
	 */
	@Override
	public Prova buscarPorId(Long id) {
		return provaDao.findOne(id);
	}

	/**
	 * 
	 */
	@Override
	public List<Prova> buscarTodos() {
		return provaDao.findAll();
	}

	/**
	 * 
	 */
	@Override
	@Transactional
	public void remover(Prova prova) {
		provaDao.delete(prova);
	}
}
