package br.com.hyperactive.sge.service.negocio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hyperactive.sge.data.dao.TreinamentoDao;
import br.com.hyperactive.sge.domain.entity.Nivel;
import br.com.hyperactive.sge.domain.entity.Treinamento;
import br.com.hyperactive.sge.service.negocio.TreinamentoService;

/**
 * 
 * @author André
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@Service
public class TreinamentoServiceImpl implements TreinamentoService {

	@Autowired
	TreinamentoDao treinamentoDao;
	
	@Override
	@Transactional
	public Treinamento salvar(Treinamento treinamento) {
		
		return treinamentoDao.save(treinamento);		
	}

	@Override
	@Transactional
	public Treinamento alterar(Treinamento treinamento) {

		return treinamentoDao.save(treinamento);
	}

	@Override
	public Treinamento buscarPorId(Long id) {

		return treinamentoDao.findOne(id);
	}

	@Override
	public List<Treinamento> buscarTodos() {

		return treinamentoDao.findAll();
	}

	@Override
	@Transactional
	public void remover(Treinamento treinamento) {
		
		treinamentoDao.delete(treinamento);
	}

	@Override
	public List<Treinamento> buscarPorNivel(Nivel nivel) {
		return treinamentoDao.findByNivel(nivel);
	}
}
