/**
 * 
 */
package br.com.hyperactive.sge.service.negocio.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hyperactive.sge.data.dao.DemandaDao;
import br.com.hyperactive.sge.domain.entity.Demanda;
import br.com.hyperactive.sge.service.negocio.DemandaService;

/**
 * @author edsf
 *
 */
@Service("demandaService")
public class DemandaServiceImpl implements DemandaService {

	/**
	 * 
	 */
	@Autowired
	private DemandaDao demandaDao;
	
	/* (non-Javadoc)
	 * @see br.com.hyperactive.sge.service.negocio.DemandaService#buscarTodos()
	 */
	public List<Demanda> buscarTodos() {
		
		return StreamSupport.stream(demandaDao.findAll().spliterator(), false)
                .collect(Collectors.toList());
	}
	
	@Transactional
	public Demanda criar(Demanda demanda) {
		//TODO: Verificar se a demanda a ser criada já existe.
		return demandaDao.save(demanda);
	}

	@Override
	@Transactional
	public Demanda alterar(Demanda demanda) {
		
		//TODO: Verificar se a demanda existe mesmo.
		return demandaDao.save(demanda);
	}

}
