/**
 * 
 */
package br.com.hyperactive.sge.service.negocio.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hyperactive.sge.data.dao.ClienteDao;
import br.com.hyperactive.sge.domain.entity.Cliente;
import br.com.hyperactive.sge.service.negocio.ClienteService;

/**
 * @author edsf
 *
 */
@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteDao clienteDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.hyperactive.sge.service.negocio.ClienteService#buscarTodos()
	 */
	@Override
	public List<Cliente> buscarTodos() {
		return StreamSupport.stream(clienteDao.findAll().spliterator(), false).collect(Collectors.toList());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.hyperactive.sge.service.negocio.ClienteService#salvar(br.com.
	 * hyperactive.sge.domain.entity.Cliente)
	 */
	@Override
	@Transactional
	public Cliente salvar(Cliente cliente) {
		return clienteDao.save(cliente);
	}

}
