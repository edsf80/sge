/**
 * 
 */
package br.com.hyperactive.sge.service.negocio.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import br.com.hyperactive.sge.domain.entity.Aluno;
import br.com.hyperactive.sge.domain.entity.Conta;
import br.com.hyperactive.sge.service.negocio.LoginService;

/**
 * @author edsf
 *
 */
@Service("loginService")
public class LoginServiceImpl implements LoginService {

	@Autowired
	private AuthenticationManager authenticationManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.hyperactive.sge.service.negocio.LoginService#
	 * efetuarLogin(java.lang.String, java.lang.String)
	 */
	public Aluno efetuarLogin(String login, String senha) {

		Authentication authentication = this.criarAutenticacao(login, senha);

		if (authentication == null) {
			return null;
		}

		SecurityContextHolder.getContext().setAuthentication(authentication);

		UserDetails userDetails = (UserDetails) authentication.getPrincipal();

		Aluno aluno = new Aluno();
		aluno.setMatricula(userDetails.getUsername());
		aluno.setNome(userDetails.getUsername());
		Conta conta = new Conta();
		conta.setLogin(userDetails.getUsername());
		List<String> papeis = new ArrayList<>();

		for (GrantedAuthority grantedAuthority : userDetails.getAuthorities()) {
			papeis.add(grantedAuthority.getAuthority());
		}
		conta.setPapeis(papeis);
		aluno.setConta(conta);

		return aluno;
	}

	/**
	 * @param login
	 * @param senha
	 * @return
	 */
	private Authentication criarAutenticacao(String login, String senha) {

		Authentication authentication = null;

		try {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, senha);

			authentication = this.authenticationManager.authenticate(token);

		} catch (BadCredentialsException bce) {
			return null;
		}

		return authentication;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.hyperactive.sge.service.negocio.LoginService#buscarUsuarioLogado()
	 */
	@Override
	public Aluno buscarUsuarioLogado() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		Aluno aluno = null;

		if (!authentication.getPrincipal().equals("anonymousUser")) {
			UserDetails userDetails = (UserDetails) authentication.getPrincipal();

			aluno = new Aluno();
			aluno.setMatricula(userDetails.getUsername());
			aluno.setNome(userDetails.getUsername());
			Conta conta = new Conta();
			conta.setLogin(userDetails.getUsername());
			List<String> papeis = new ArrayList<>();

			for (GrantedAuthority grantedAuthority : userDetails.getAuthorities()) {
				papeis.add(grantedAuthority.getAuthority());
			}
			conta.setPapeis(papeis);
			aluno.setConta(conta);
		}

		return aluno;
	}

}
