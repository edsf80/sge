package br.com.hyperactive.sge.service.negocio;

import java.util.List;

import br.com.hyperactive.sge.domain.entity.Demanda;

public interface DemandaService {

	List<Demanda> buscarTodos();
	
	Demanda criar(Demanda demanda);
	
	Demanda alterar(Demanda demanda);
}
