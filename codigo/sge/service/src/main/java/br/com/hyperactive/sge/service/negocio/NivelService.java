/**
 * 
 */
package br.com.hyperactive.sge.service.negocio;

import java.util.List;

import br.com.hyperactive.sge.domain.entity.Nivel;

/**
 * @author edsf
 *
 */
public interface NivelService {

	/**
	 * @return
	 */
	List<Nivel> buscarTodos();
	
	/**
	 * @param nivel
	 * @return
	 */
	Nivel salvar(Nivel nivel);
}
