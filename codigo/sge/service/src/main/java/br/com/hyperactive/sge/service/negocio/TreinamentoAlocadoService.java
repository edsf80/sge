package br.com.hyperactive.sge.service.negocio;

import java.util.List;

import br.com.hyperactive.sge.domain.entity.TreinamentoAlocado;

/**
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
public interface TreinamentoAlocadoService {
	
	TreinamentoAlocado salvar(TreinamentoAlocado treinamentoAlocado);
	
	void apagar(Long id);
	
	List<TreinamentoAlocado> buscarPorMatricula(String matricula);

}
