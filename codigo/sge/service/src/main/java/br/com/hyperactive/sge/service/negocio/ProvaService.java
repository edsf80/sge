package br.com.hyperactive.sge.service.negocio;

import java.util.List;

import br.com.hyperactive.sge.domain.entity.Prova;

/**
 * 
 * <p>
 * <b> Prova Service </b>
 * </p>
 *
 * <p>
 * 	Define metódos para serviços providos pela entidade @Prova.
 * </p>
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>	
 *
 */
public interface ProvaService {
	
	/**
	 * 
	 * @param prova
	 * @return
	 */
	Prova salvar(Prova prova);
	
	/**
	 * 
	 * @param prova
	 * @return
	 */
	Prova alterar(Prova prova);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	Prova buscarPorId(Long id);
	
	/**
	 * 
	 * @return
	 */
	List<Prova> buscarTodos();
	
	/**
	 * 
	 * @param prova
	 */
	void remover(Prova prova);
}
