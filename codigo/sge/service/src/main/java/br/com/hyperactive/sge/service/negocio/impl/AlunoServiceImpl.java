/**
 * 
 */
package br.com.hyperactive.sge.service.negocio.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hyperactive.sge.data.dao.AdmissaoDao;
import br.com.hyperactive.sge.data.dao.AlunoDao;
import br.com.hyperactive.sge.domain.entity.Aluno;
import br.com.hyperactive.sge.service.negocio.AlunoService;
import br.com.hyperactive.sge.service.negocio.NivelAlunoService;

/**
 * @author edsf
 *
 */
@Service
public class AlunoServiceImpl implements AlunoService {

	@Autowired
	private AlunoDao alunoDao;
	
	@Autowired
	private AdmissaoDao admissaoDao;
	
	@Autowired
	private NivelAlunoService nivelAlunoService;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.hyperactive.sge.service.negocio.AlunoService#buscarTodos()
	 */
	@Override
	public List<Aluno> buscarTodos() {

		return alunoDao.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.hyperactive.sge.service.negocio.AlunoService#buscarPorMatricula(
	 * java.lang.String)
	 */
	@Override
	public Aluno buscarPorMatricula(String matricula) {
		
		Aluno aluno = alunoDao.findByMatricula(matricula);
		aluno.setAdmissoes(admissaoDao.findByMatricula(matricula));
		aluno.setNiveis(nivelAlunoService.buscarPorMatricula(matricula));
		
		return aluno;
	}

}
