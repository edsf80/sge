package br.com.hyperactive.sge.service.negocio.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hyperactive.sge.data.dao.TreinamentoAlocadoDao;
import br.com.hyperactive.sge.domain.entity.TreinamentoAlocado;
import br.com.hyperactive.sge.service.negocio.TreinamentoAlocadoService;

/**
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@Service
public class TreinamentoAlocadoServiceImpl implements TreinamentoAlocadoService{
	
	@Autowired
	private TreinamentoAlocadoDao treinamentoAlocadoDao;

	@Transactional
	@Override
	public TreinamentoAlocado salvar(TreinamentoAlocado treinamentoAlocado) {
		return treinamentoAlocadoDao.save(treinamentoAlocado);
	}

	@Transactional
	@Override
	public void apagar(Long id) {
		treinamentoAlocadoDao.delete(id);
	}

	@Override
	public List<TreinamentoAlocado> buscarPorMatricula(String matricula) {
		return treinamentoAlocadoDao.findByMatricula(matricula);
	}
}
