/**
 * 
 */
package br.com.hyperactive.sge.service.negocio.impl;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author edsf
 *
 */
public class UserDetailsImpl implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4127773916346149593L;

	private String username;

	private String password;

	private String nome;

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		// TODO Auto-generated method stub
		return this.password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		// TODO Auto-generated method stub
		return this.username;
	}

	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return Boolean.TRUE;
	}

	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return Boolean.TRUE;
	}

	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return Boolean.TRUE;
	}

	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return Boolean.TRUE;
	}
}
