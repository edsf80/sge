/**
 * 
 */
package br.com.hyperactive.sge.service.negocio.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hyperactive.sge.data.dao.NivelDao;
import br.com.hyperactive.sge.domain.entity.Nivel;
import br.com.hyperactive.sge.service.negocio.NivelService;

/**
 * @author edsf
 *
 */
@Service
public class NivelServiceImpl implements NivelService {

	@Autowired
	private NivelDao nivelDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.hyperactive.sge.service.negocio.NivelService#buscarTodos()
	 */
	@Override
	public List<Nivel> buscarTodos() {

		return StreamSupport.stream(nivelDao.findAll().spliterator(), false).collect(Collectors.toList());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.hyperactive.sge.service.negocio.NivelService#salvar(br.com.
	 * hyperactive.sge.domain.entity.Nivel)
	 */
	@Override
	public Nivel salvar(Nivel nivel) {

		return nivelDao.save(nivel);
	}

}
