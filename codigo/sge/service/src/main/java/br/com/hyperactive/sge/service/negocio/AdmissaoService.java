/**
 * 
 */
package br.com.hyperactive.sge.service.negocio;

import br.com.hyperactive.sge.domain.entity.Admissao;

/**
 * @author edsf
 *
 */
public interface AdmissaoService {

	Admissao criar(Admissao admissao);
	
	Admissao alterar(Admissao admissao);
}
