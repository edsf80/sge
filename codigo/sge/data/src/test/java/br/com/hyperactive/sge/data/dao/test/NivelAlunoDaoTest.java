/**
 * 
 */
package br.com.hyperactive.sge.data.dao.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import br.com.hyperactive.sge.data.dao.NivelAlunoDao;
import br.com.hyperactive.sge.domain.entity.NivelAluno;

import static org.junit.Assert.assertNotNull;

/**
 * @author edsf
 *
 */
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
	TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringRunner.class)
@DataJpaTest
@DatabaseSetup("/nivel-entries.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { "/nivel-entries.xml" })
@DirtiesContext
public class NivelAlunoDaoTest {
	
	@Autowired
	private NivelAlunoDao nivelAlunoDao;
	
	@Test
	public void testFindByMatricula() {
		List<NivelAluno> niveis = nivelAlunoDao.findByMatricula("12345");
		assertNotNull(niveis);
	}

}
