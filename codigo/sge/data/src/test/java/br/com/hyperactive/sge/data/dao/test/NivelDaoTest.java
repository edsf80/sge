/**
 * 
 */
package br.com.hyperactive.sge.data.dao.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import br.com.hyperactive.sge.data.dao.NivelDao;
import br.com.hyperactive.sge.domain.entity.Nivel;

/**
 * @author edsf
 *
 */
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@RunWith(SpringRunner.class)
@DataJpaTest
@DatabaseSetup("/nivel-entries.xml")
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { "/nivel-entries.xml" })
@DirtiesContext
public class NivelDaoTest {

	@Autowired
	private NivelDao nivelDao;
	
	@Autowired
	private LocalValidatorFactoryBean validator;
	
	@Test
	public void testFindAll() {
		List<Object> niveis = StreamSupport.stream(nivelDao.findAll().spliterator(), false)
				.collect(Collectors.toList());
		assertNotNull(niveis);
		assertTrue(niveis.size() > 1);
		Nivel nivel = (Nivel) niveis.get(0);
		assertTrue(nivel.getId().longValue() == 1);
		
	}
	
	@Test
	public void testSave() {
		Nivel nivel = new Nivel();
		nivel.setDescricao("Descricao de inserção de nivel");
		nivelDao.save(nivel);
		assertTrue(nivel.getId() != null);		
	}
	
	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void testSaveParametroNulo() {
		nivelDao.save((Nivel) null);
	}
	
	@Test
	public void testNivelComDescricaoNula() {
		Nivel nivel = new Nivel();
		assertTrue(validator.validate(nivel).size() > 0);
	}
	
	@Test
	public void testSaveComIdInexistente() {
		Nivel nivel = new Nivel();
		nivel.setId(5l);
		nivel.setDescricao("Descricao de inserção de nivel");
		nivelDao.save(nivel);
		assertTrue(nivel.getId().equals(5l));		
	}
}
