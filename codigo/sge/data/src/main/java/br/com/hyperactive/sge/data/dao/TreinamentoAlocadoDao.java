package br.com.hyperactive.sge.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.hyperactive.sge.domain.entity.TreinamentoAlocado;

/**
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@Repository
public interface TreinamentoAlocadoDao extends JpaRepository<TreinamentoAlocado, Long>{
	
	List<TreinamentoAlocado> findByMatricula(String matricula);

}
