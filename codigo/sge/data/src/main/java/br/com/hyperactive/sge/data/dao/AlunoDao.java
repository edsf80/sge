/**
 * 
 */
package br.com.hyperactive.sge.data.dao;

import java.util.List;

import br.com.hyperactive.sge.domain.entity.Aluno;

/**
 * @author edsf
 *
 */
public interface AlunoDao {

	List<Aluno> findAll();
	
	Aluno findByMatricula(String matricula);
}
