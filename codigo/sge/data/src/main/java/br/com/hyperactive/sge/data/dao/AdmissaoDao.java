package br.com.hyperactive.sge.data.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.hyperactive.sge.domain.entity.Admissao;

public interface AdmissaoDao extends CrudRepository<Admissao, Long> {

	List<Admissao> findByMatricula(String matricula);
}
