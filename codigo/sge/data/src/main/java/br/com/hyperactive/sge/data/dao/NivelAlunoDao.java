/**
 * 
 */
package br.com.hyperactive.sge.data.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.hyperactive.sge.domain.entity.NivelAluno;

/**
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@Repository
public interface NivelAlunoDao extends JpaRepository<NivelAluno, Long> {

	List<NivelAluno> findByMatricula(String matricula);
	
	List<NivelAluno> findByPrazoConclusaoBetween(Date inicio, Date fim);
}
