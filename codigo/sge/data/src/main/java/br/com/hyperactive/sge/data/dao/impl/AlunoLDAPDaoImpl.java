/**
 * 
 */
package br.com.hyperactive.sge.data.dao.impl;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.stereotype.Repository;

import br.com.hyperactive.sge.data.dao.AlunoDao;
import br.com.hyperactive.sge.domain.entity.Aluno;

/**
 * @author edsf
 *
 */
@Repository
public class AlunoLDAPDaoImpl implements AlunoDao {

	@Autowired
	private LdapTemplate ldapTemplate;

	private class AlunoAttributesMapper implements AttributesMapper<Aluno> {
		public Aluno mapFromAttributes(Attributes attrs) throws NamingException {

			Aluno aluno = new Aluno();
			aluno.setMatricula(attrs.get("uid") == null ? null : (String) attrs.get("uid").get());
			aluno.setNome(attrs.get("displayName") == null ? null : (String) attrs.get("displayName").get());
			aluno.setEmail(attrs.get("mail") == null ? null : (String) attrs.get("mail").get());

			return aluno;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.hyperactive.sge.data.dao.AlunoDao#findAll()
	 */
	public List<Aluno> findAll() {	

		return ldapTemplate.search(query().base("ou=Users").where("objectclass").is("person"),
				new AlunoAttributesMapper());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.hyperactive.sge.data.dao.AlunoDao#findByMatricula(java.lang.
	 * String)
	 */
	public Aluno findByMatricula(String matricula) {

		AndFilter andFilter = new AndFilter();
		andFilter.and(new EqualsFilter("objectclass", "person"));
		andFilter.and(new EqualsFilter("uid", matricula));
		List<Aluno> resultado = ldapTemplate.search("", andFilter.encode(), new AlunoAttributesMapper());

		return resultado.get(0);
	}

}
