/**
 * 
 */
package br.com.hyperactive.sge.data.dao;

import org.springframework.data.repository.Repository;

import br.com.hyperactive.sge.domain.entity.Usuario;

/**
 * @author edsf
 *
 */
public interface UsuarioDao extends Repository<Usuario, Long> {

	Usuario findByLogin(String login);
}
