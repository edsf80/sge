/**
 * 
 */
package br.com.hyperactive.sge.data.dao;

import org.springframework.data.repository.CrudRepository;

import br.com.hyperactive.sge.domain.entity.Nivel;

/**
 * @author edsf
 *
 */
public interface NivelDao extends CrudRepository<Nivel, Long> {

}
