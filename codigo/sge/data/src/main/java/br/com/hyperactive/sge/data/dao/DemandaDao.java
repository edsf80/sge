/**
 * 
 */
package br.com.hyperactive.sge.data.dao;

import org.springframework.data.repository.CrudRepository;

import br.com.hyperactive.sge.domain.entity.Demanda;

/**
 * @author edsf
 *
 */
public interface DemandaDao extends CrudRepository<Demanda, Long> {

}
