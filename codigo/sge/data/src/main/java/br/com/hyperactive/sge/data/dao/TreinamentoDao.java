package br.com.hyperactive.sge.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.hyperactive.sge.domain.entity.Nivel;
import br.com.hyperactive.sge.domain.entity.Treinamento;

/**
 * 
 * @author amsv
 *
 */
public interface TreinamentoDao extends JpaRepository<Treinamento, Long>{
	
	List<Treinamento> findByNivel(Nivel nivel);

}
