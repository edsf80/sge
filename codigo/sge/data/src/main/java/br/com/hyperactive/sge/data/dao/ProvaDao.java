package br.com.hyperactive.sge.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.hyperactive.sge.domain.entity.Prova;

/**
 * 
 * <p>
 * <b>Prova DAO</b>
 * </p>
 *
 * <p>
 * 	Repositório de uma prova.
 * </p>
 *
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>	
 *
 */
@Repository
public interface ProvaDao extends JpaRepository<Prova, Long>{

}
