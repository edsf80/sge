/**
 * 
 */
package br.com.hyperactive.sge.data.dao;

import org.springframework.data.repository.CrudRepository;

import br.com.hyperactive.sge.domain.entity.Cliente;

/**
 * @author edsf
 *
 */
public interface ClienteDao extends CrudRepository<Cliente, Long> {

}
