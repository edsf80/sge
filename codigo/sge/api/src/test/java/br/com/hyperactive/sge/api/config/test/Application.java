/**
 * 
 */
package br.com.hyperactive.sge.api.config.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.test.EmbeddedLdapServerFactoryBean;
import org.springframework.ldap.test.LdifPopulator;


/**
 * @author edsf
 *
 */
@SpringBootApplication
//@ComponentScan(basePackages = { "br.com.hyperactive.sge" })
public class Application extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean(name = "contextSoure")
	@ConfigurationProperties(prefix = "ldap.contextSource")
	public LdapContextSource contextSource() {
		
		LdapContextSource context = new LdapContextSource();
		
		return context;
	}
	
	@Bean
	@DependsOn({"embeddedLdapServer", "contextSoure"})
	public LdifPopulator ldifPopulator(ContextSource contextSource) {
		LdifPopulator ldifPopulator = new LdifPopulator();
		ldifPopulator.setContextSource(contextSource);
		ldifPopulator.setResource(new ClassPathResource("alunosads.ldif"));
		ldifPopulator.setBase("dc=ifpb,dc=mont,dc=edu,dc=br");
		ldifPopulator.setClean(true);
		ldifPopulator.setDefaultBase("ou=Users,dc=ifpb,dc=mont,dc=edu,dc=br");
		return ldifPopulator;
	}

	@Bean(name = "embeddedLdapServer")
	public EmbeddedLdapServerFactoryBean embeddedLdapServerFactoryBean() {
		EmbeddedLdapServerFactoryBean embeddedLdapServerFactoryBean = new EmbeddedLdapServerFactoryBean();
		embeddedLdapServerFactoryBean.setPartitionName("sge");
		embeddedLdapServerFactoryBean.setPartitionSuffix("dc=ifpb,dc=mont,dc=edu,dc=br");
		embeddedLdapServerFactoryBean.setPort(18880);
		return embeddedLdapServerFactoryBean;
}

	@Bean
	public LdapTemplate ldapTemplate(ContextSource contextSource) {
		return new LdapTemplate(contextSource);
	}
}
