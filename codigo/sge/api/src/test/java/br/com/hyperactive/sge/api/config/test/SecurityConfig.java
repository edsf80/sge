/**
 * 
 */
package br.com.hyperactive.sge.api.config.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * @author edsf
 *
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig {

	@Autowired
	private LdapContextSource contextSource;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		BindAuthenticator authenticator = new BindAuthenticator(contextSource);
		authenticator.setUserDnPatterns(new String[] { "uid={0}, ou=Users" });
		DefaultLdapAuthoritiesPopulator dlap = new DefaultLdapAuthoritiesPopulator(contextSource, "ou=Groups");
		dlap.setGroupRoleAttribute("cn");
		dlap.setGroupSearchFilter("(memberUid={1})");

		auth.authenticationProvider(new LdapAuthenticationProvider(authenticator, dlap));
	}

	@Configuration
	public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

		@Autowired
		private LogoutSuccessHandler logoutSuccessHandler;

		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable().authorizeRequests()
					.antMatchers("/services/authentication.service.js", "/services/httpinterceptor.service.js",
							"/plugins/**", "/js/*", "/css/*", "/login", "/index.html", "/logout", "/user",
							"/components/login/index.controller.js", "/components/login/index.view.html", "/components/directives/access.directive.js")
					.permitAll()
					.antMatchers("/components/admissao/**", "/admissao").hasRole("ADMINISTRATORS")
					.antMatchers("/components/demanda/modal.controller.js", "/components/demanda/modal.view.js").hasRole("ADMINISTRATORS")
					.anyRequest().authenticated().and().logout().logoutSuccessHandler(logoutSuccessHandler);
		}
	}
}
