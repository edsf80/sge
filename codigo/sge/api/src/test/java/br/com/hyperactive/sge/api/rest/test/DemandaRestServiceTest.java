/**
 * 
 */
package br.com.hyperactive.sge.api.rest.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.hyperactive.sge.api.config.test.Application;

/**
 * @author edsf
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes=Application.class)
@TestPropertySource(locations="classpath:application.properties")
public class DemandaRestServiceTest {
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	@Ignore
	@WithUserDetails
	public void exemploTest() {
		String body = this.restTemplate.getForObject("/demanda", String.class);
		System.out.println(body);
        assertThat(body).isEqualTo("Hello World");
	}

}
