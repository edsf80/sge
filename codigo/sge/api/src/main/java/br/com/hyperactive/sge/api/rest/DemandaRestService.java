package br.com.hyperactive.sge.api.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.hyperactive.sge.domain.entity.Demanda;
import br.com.hyperactive.sge.service.negocio.DemandaService;

@RestController
@RequestMapping(value = "/demanda")
public class DemandaRestService {

	/**
	 * 
	 */
	@Autowired
	private DemandaService demandaService;

	/**
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<Demanda> buscarTodos() {

		return demandaService.buscarTodos();
	}

	/**
	 * @param demanda
	 * @return
	 */
	@PreAuthorize("ROLE_ADMINISTRATORS")
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Demanda criarDemanda(@Valid @RequestBody Demanda demanda) {

		demanda = demandaService.criar(demanda);

		return demanda;
	}
	
	/**
	 * @param demanda
	 * @return
	 */
	@PreAuthorize("ROLE_ADMINISTRATORS")
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody Demanda alterarDemanda(@Valid @RequestBody Demanda demanda) {

		demanda = demandaService.alterar(demanda);

		return demanda;
	}
}
