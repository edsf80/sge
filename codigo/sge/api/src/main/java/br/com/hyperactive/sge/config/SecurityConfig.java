/**
 * 
 */
package br.com.hyperactive.sge.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig {

	@Autowired
	private LdapContextSource contextSource;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		BindAuthenticator authenticator = new BindAuthenticator(contextSource);
		authenticator.setUserDnPatterns(new String[] { "uid={0}, ou=Users" });
		DefaultLdapAuthoritiesPopulator dlap = new DefaultLdapAuthoritiesPopulator(contextSource, "ou=Groups");
		dlap.setGroupRoleAttribute("cn");
		dlap.setGroupSearchFilter("(memberUid={1})");

		auth.authenticationProvider(new LdapAuthenticationProvider(authenticator, dlap));
	}

	@Configuration
	public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

		@Autowired
		private LogoutSuccessHandler logoutSuccessHandler;

		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable().authorizeRequests()
			
					.antMatchers("/plugins/**", "/js/*", "/css/*", "/login", "/index.html", "/logout", "/user", "/components/public/**", "/img/*")
					.permitAll()
					
					.antMatchers("/components/private/nivel/**", "/nivel").hasRole("ADMINISTRATORS")
					.antMatchers("/components/private/cliente/**", "/cliente", "/components/private/admissao/**", "/admissao").hasRole("ADMINISTRATORS")
					.antMatchers("/components/private/treinamento/index.edit.controller.js", "/components/private/treinamento/index.edit.view.html").hasRole("ADMINISTRATORS")
					.antMatchers("/components/private/prova/index.edit.controller.js", "/components/private/prova/index.edit.view.html").hasRole("ADMINISTRATORS")

					.antMatchers(HttpMethod.POST, "/treinamento", "/prova", "/demanda").hasAnyRole("ADMINISTRATORS")
					.antMatchers(HttpMethod.PUT, "/treinamento", "/prova", "/demanda").hasAnyRole("ADMINISTRATORS")
					.antMatchers(HttpMethod.DELETE, "/treinamento", "/prova", "/demanda").hasAnyRole("ADMINISTRATORS")
					
					.anyRequest().authenticated().and().logout().logoutSuccessHandler(logoutSuccessHandler);
			
	        http.exceptionHandling().accessDeniedHandler(accessDeniedHandler());

		}
		
	    private AccessDeniedHandler accessDeniedHandler() {
	        return (request, response, ex) ->
	            response.sendError(HttpServletResponse.SC_FORBIDDEN, ex.getLocalizedMessage());
	    }
	}
}
