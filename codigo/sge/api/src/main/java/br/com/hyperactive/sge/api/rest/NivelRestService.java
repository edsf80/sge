/**
 * 
 */
package br.com.hyperactive.sge.api.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.hyperactive.sge.domain.entity.Nivel;
import br.com.hyperactive.sge.service.negocio.NivelService;

/**
 * @author edsf
 *
 */
@RestController
@RequestMapping(value ="/nivel")
public class NivelRestService {

	/**
	 * 
	 */
	@Autowired
	private NivelService nivelService;
	
	/**
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<Nivel> buscarTodos() {
		return nivelService.buscarTodos();
	}
	
	/**
	 * @param nivel
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Nivel criar(@Valid @RequestBody Nivel nivel) {
		return nivelService.salvar(nivel);
	}
	
	/**
	 * @param nivel
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody Nivel atualizar(@Valid @RequestBody Nivel nivel) {
		return nivelService.salvar(nivel);
	}
}
