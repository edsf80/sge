/**
 * 
 */
package br.com.hyperactive.sge.api.rest;

import org.springframework.stereotype.Component;

/**
 * @author edsf
 *
 */
@Component
public class Config {
	
	//@Value("${nome}")
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
