/**
 * 
 */
package br.com.hyperactive.sge.api.rest;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.hyperactive.sge.domain.entity.NivelAluno;
import br.com.hyperactive.sge.service.negocio.NivelAlunoService;

/**
 * @author edsf
 *
 */
@RestController
@RequestMapping(value = "/nivelaluno")
public class NivelAlunoRestService {

	/**
	 * 
	 */
	@Autowired
	private NivelAlunoService nivelAlunoService;

	/**
	 * @param nivelAluno
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody NivelAluno criar(@Valid @RequestBody NivelAluno nivelAluno) {
		return nivelAlunoService.salvar(nivelAluno);
	}

	/**
	 * @param nivelAluno
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody NivelAluno alterar(@Valid @RequestBody NivelAluno nivelAluno) {
		return nivelAlunoService.salvar(nivelAluno);
	}

	/**
	 * @param id
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void remover(@NotNull @PathVariable Long id) {
		nivelAlunoService.apagar(id);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{matricula}")
	public ResponseEntity<List<NivelAluno>> buscarPorAluno(@NotNull @PathVariable String matricula) {
		List<NivelAluno> niveisAlunos = nivelAlunoService.buscarPorMatricula(matricula);
		return new ResponseEntity<List<NivelAluno>>(niveisAlunos, HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<NivelAluno>> buscarTodos() {
		List<NivelAluno> niveisAlunos = nivelAlunoService.buscarTodos();
		return new ResponseEntity<List<NivelAluno>>(niveisAlunos, HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param prazo
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value="/prazos")
	public ResponseEntity<List<NivelAluno>> buscarPorPrazo(@NotNull @RequestParam("inicio") @DateTimeFormat(pattern = "dd-MM-yyyy") 
												Date incio, @RequestParam("fim") @DateTimeFormat(pattern = "dd-MM-yyyy") Date fim) {
		
		List<NivelAluno> niveis = nivelAlunoService.buscarPorPrazo(incio, fim);
		
		return new ResponseEntity<>(niveis, HttpStatus.OK);
		
	}

}
