package br.com.hyperactive.sge.api.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.hyperactive.sge.domain.entity.Cliente;
import br.com.hyperactive.sge.service.negocio.ClienteService;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteRestService {

	/**
	 * 
	 */
	@Autowired
	private ClienteService clienteService;
	
	/**
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<Cliente> buscarTodos() {

		return clienteService.buscarTodos();
	}
	
	@PreAuthorize("ROLE_ADMINISTRATORS")
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Cliente criarCliente(@Valid @RequestBody Cliente cliente) {

		return clienteService.salvar(cliente);
	}
	
	/**
	 * @param demanda
	 * @return
	 */
	@PreAuthorize("ROLE_ADMINISTRATORS")
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody Cliente alterarCliente(@Valid @RequestBody Cliente cliente) {
		return clienteService.salvar(cliente);
	}
}
