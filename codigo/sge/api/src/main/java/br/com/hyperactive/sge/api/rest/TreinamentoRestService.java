package br.com.hyperactive.sge.api.rest;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.hyperactive.sge.domain.entity.Nivel;
import br.com.hyperactive.sge.domain.entity.Treinamento;
import br.com.hyperactive.sge.service.negocio.TreinamentoService;

/**
 * Rest Controller responsável por disponibilizar serviços relacionados a um
 * treinamento.
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@RestController
@RequestMapping(value = "/treinamento")
public class TreinamentoRestService {

	@Autowired
	private TreinamentoService treinamentoService;

	/**
	 * end poit salva um novo treinamento.
	 * 
	 * @param treinamento
	 * 
	 * @return Http CREATED
	 */

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Treinamento> salvar(@Valid @RequestBody Treinamento treinamento) {

		Treinamento treinamentoCriado = treinamentoService.salvar(treinamento);

		return new ResponseEntity<>(treinamentoCriado, HttpStatus.CREATED);

	}

	/**
	 * end point quando chamado atualiza um treinamento.
	 * 
	 * @param treinamento
	 * 
	 * @return treinamento atualizado.
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Treinamento> atualizar(@Valid @RequestBody Treinamento treinamento) {

		Treinamento treinamentoAtualizado = treinamentoService.alterar(treinamento);

		return new ResponseEntity<Treinamento>(treinamentoAtualizado, HttpStatus.OK);
	}

	/**
	 * end point quando chamado remove um treinamento passado por paramentro do
	 * banco de dados.
	 * 
	 * @param treinamento
	 *            - treinamento a ser removido
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE, value="/{id}")
	public ResponseEntity<Treinamento> remover(@NotNull @PathVariable Long id) {
		
		Treinamento treinamento = new Treinamento();
		treinamento.setId(id);
		treinamentoService.remover(treinamento);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * busca todos os treinamentos.
	 * 
	 * @return lista de treinamentos
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Treinamento>> buscarTodos() {

		List<Treinamento> treinamentos = treinamentoService.buscarTodos();

		return new ResponseEntity<List<Treinamento>>(treinamentos, HttpStatus.OK);
	}

	/**
	 * 
	 * @param idNivel
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{idNivel}")
	public ResponseEntity<List<Treinamento>> buscarPorNivel(@NotNull @PathVariable Long idNivel) {

		Nivel nivel = new Nivel();
		nivel.setId(idNivel);
		List<Treinamento> treinamentos = treinamentoService.buscarPorNivel(nivel);

		return new ResponseEntity<List<Treinamento>>(treinamentos, HttpStatus.OK);
	}
}
