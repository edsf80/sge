package br.com.hyperactive.sge.api.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.hyperactive.sge.domain.entity.Admissao;
import br.com.hyperactive.sge.service.negocio.AdmissaoService;

@RestController
@RequestMapping(value = "/admissao")
public class AdmissaoRestService {

	/**
	 * 
	 */
	@Autowired
	private AdmissaoService admissaoService;

	/**
	 * @param admissao
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody Admissao criar(@RequestBody Admissao admissao) {
		Admissao admissaoNova = admissaoService.criar(admissao); 
		
		return admissaoNova;
	}
	
	/**
	 * @param admissao
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody Admissao alterar(@RequestBody Admissao admissao) {
		return admissaoService.alterar(admissao);
	}
}
