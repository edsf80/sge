/**
 * 
 */
package br.com.hyperactive.sge.api.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.hyperactive.sge.domain.entity.Aluno;
import br.com.hyperactive.sge.domain.entity.Conta;
import br.com.hyperactive.sge.service.negocio.LoginService;

/**
 * @author edsf
 *
 */
@RestController
public class LoginRestService {
	
	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Object> executarLogin(@RequestBody Conta conta) {

		Aluno aluno = loginService.efetuarLogin(conta.getLogin(), conta.getSenha());		
		
		ResponseEntity<Object> responseEntity = null;
		
		if(aluno == null) {
			responseEntity = new ResponseEntity<Object>("Usuário ou senha inválida", HttpStatus.UNPROCESSABLE_ENTITY);
		} else {			
			responseEntity = new ResponseEntity<Object>(aluno, HttpStatus.OK);
		}

		return responseEntity;
	}
	
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Aluno> usuarioLogado() {

		Aluno aluno = loginService.buscarUsuarioLogado();		
		
		ResponseEntity<Aluno> responseEntity = new ResponseEntity<Aluno>(aluno, HttpStatus.OK);		

		return responseEntity;
	}
}
