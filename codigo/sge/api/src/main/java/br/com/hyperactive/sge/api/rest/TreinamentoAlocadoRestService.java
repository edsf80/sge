package br.com.hyperactive.sge.api.rest;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.hyperactive.sge.domain.entity.TreinamentoAlocado;
import br.com.hyperactive.sge.service.negocio.TreinamentoAlocadoService;

/**
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@RestController
@RequestMapping(value = "/treinamentoAlocado")
public class TreinamentoAlocadoRestService {
	
	@Autowired
	private TreinamentoAlocadoService treinamentoAlocadoService;
	
	
	/**
	 * @param TreinamentoAlocado
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody TreinamentoAlocado criar(@Valid @RequestBody TreinamentoAlocado TreinamentoAlocado) {
		return treinamentoAlocadoService.salvar(TreinamentoAlocado);
	}

	/**
	 * @param TreinamentoAlocado
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody TreinamentoAlocado alterar(@Valid @RequestBody TreinamentoAlocado TreinamentoAlocado) {
		return treinamentoAlocadoService.salvar(TreinamentoAlocado);
	}

	/**
	 * @param id
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void remover(@NotNull @PathVariable Long id) {
		treinamentoAlocadoService.apagar(id);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{matricula}")
	public ResponseEntity<List<TreinamentoAlocado>> buscarPorAluno(@NotNull @PathVariable String matricula) {
		List<TreinamentoAlocado> niveisAlunos = treinamentoAlocadoService.buscarPorMatricula(matricula);
		return new ResponseEntity<List<TreinamentoAlocado>>(niveisAlunos, HttpStatus.OK);
	}

}
