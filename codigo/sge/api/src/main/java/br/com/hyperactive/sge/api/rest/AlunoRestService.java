/**
 * 
 */
package br.com.hyperactive.sge.api.rest;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.hyperactive.sge.domain.entity.Aluno;
import br.com.hyperactive.sge.service.negocio.AlunoService;

/**
 * @author edsf
 *
 */
@RestController
@RequestMapping(value = "/aluno")
public class AlunoRestService {

	@Autowired
	private AlunoService alunoService;
	
	@RequestMapping(value = "/{matricula}", method = RequestMethod.GET)
	public @ResponseBody Aluno buscarPorMatricula(@NotNull @PathVariable String matricula) {
		return alunoService.buscarPorMatricula(matricula);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<Aluno> buscarTodos() {
		return alunoService.buscarTodos();
	}
}
