package br.com.hyperactive.sge.api.rest;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.hyperactive.sge.domain.entity.Prova;
import br.com.hyperactive.sge.service.negocio.ProvaService;

/**
 * 
 * <p>
 * <b>Rest Controller de uma Prova</b>
 * </p>
 *
 * <p>
 * Rest Controller responsável por disponibilizar end points de serviços de
 * uma @Prova.
 * </p>
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@RestController
@RequestMapping(value = "/prova")
public class ProvaRestService {

	@Autowired
	private ProvaService provaService;

	/**
	 * 
	 * @param prova
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Prova> salvar(@RequestBody Prova prova) {
		Prova provaSalva = provaService.salvar(prova);
		return new ResponseEntity<Prova>(provaSalva, HttpStatus.CREATED);
	}

	/**
	 * 
	 * @param prova
	 * @return
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Prova> atualizar(@RequestBody Prova prova) {

		Prova provaAtualizada = provaService.alterar(prova);

		return new ResponseEntity<Prova>(provaAtualizada, HttpStatus.CREATED);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<Prova> remover(@NotNull @PathVariable Long id) {

		Prova prova = new Prova();
		prova.setId(id);
		provaService.remover(prova);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Prova>> buscarTodos() {

		List<Prova> provas = provaService.buscarTodos();

		return new ResponseEntity<List<Prova>>(provas, HttpStatus.OK);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<Prova> buscarPorId(@NotNull @PathVariable Long id) {
		Prova prova = provaService.buscarPorId(id);
		return new ResponseEntity<Prova>(prova, HttpStatus.OK);
	}
}
