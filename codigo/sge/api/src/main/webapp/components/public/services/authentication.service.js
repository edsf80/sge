/**
 * @ngdoc Service
 * @name authenticationService
 * 
 * @description Services para controle de autenticação
 * 
 * @author Edsf
 */
(function() {
	angular.module('sge')
	.factory('authenticationService',
					function($http, $rootScope) {
						var resultado = {};

						resultado.doLogin = function(username, password,
								callback, callbackError) {
							$http.post('login', {
								login : username,
								senha : password
							}).then(function(response) {
								$rootScope.sessionUser = response.data;
								callback();
							}, function(error) {
								if (error.status = 422) {
									callbackError(error.data);
								}
							});
						};

						resultado.doLogout = function(callback) {
							$rootScope.sessionUser = {}; 
							$http.get('logout').then(function(response) {
								callback();
							});
						};

						resultado.usuarioLogado = function(callback) {
							if (angular.equals($rootScope.sessionUser, {})) {
								callback(false); 
							} else if ($rootScope.sessionUser === undefined) {
								
								$http
										.get('user')
										.then(
												function(response) {
													$rootScope.sessionUser = response.data;
													if ($rootScope.sessionUser
															.hasOwnProperty("matricula")) {
														console.log("Pegou o usuario no servidor");
														callback(true);
													} else {
														callback(false);
													}
												});
							} else {
								callback(true);
							}
						}

						resultado.autorizado = function(authorizedRoles) {
							if (!angular.isArray(authorizedRoles)) {
								if (authorizedRoles == '*') {
									return true;
								}
								authorizedRoles = [ authorizedRoles ];
							}
							var isAuthorized = false;
							angular
									.forEach(
											authorizedRoles,
											function(authorizedRole) {
												var authorized = (!!$rootScope.sessionUser && $rootScope.sessionUser.conta.papeis
														.indexOf(authorizedRole) !== -1);
												if (authorized
														|| authorizedRole == '*') {
													isAuthorized = true;
												}
											});
							return isAuthorized;
						}

						return resultado;
					});

})();