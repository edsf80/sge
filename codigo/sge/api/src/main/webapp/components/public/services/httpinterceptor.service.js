/**
 * @ngdoc Service
 * @name errorResolverInterceptor
 * 
 * @description Service para interceptação de erros do servidor
 * 
 * @author Edsf
 */

(function(){
  var app = angular.module('sge');

  var regexIso8601 = /^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})\.(\d{1,})(Z|([\-+])(\d{2}):(\d{2}))?)?)?)?$/;

  function convertDateStringsToDates(input) {
    if (typeof input !== "object") return input;

    for (var key in input) {
        if (!input.hasOwnProperty(key)) continue;

        var value = input[key];
        var match;
        if (typeof value === "string" && (match = value.match(regexIso8601))) {
            var milliseconds = Date.parse(match[0])
            if (!isNaN(milliseconds)) {
                input[key] = new Date(milliseconds);
            }
        } else if (typeof value === "object") {
            convertDateStringsToDates(value);
        }
    }
}

  /**
   * inteceptador de erros no servidor
   */
  app.factory("errorResolverInterceptor", function($q, growl) {
    var errorResolverInterceptor = {
        responseError: function(response) {
            if (response.status >= 500) {
              growl.error("Erro na comunicação com servidor. Tente novamente mais tarde");
            }
            return $q.reject(response);
        }
    };
    return errorResolverInterceptor;
  });
  
  /**
   * inteceptador de acesso negado
   */
  app.factory("accessDeniedInterceptor", function($q, $location, growl) {
    var errorResolverInterceptor = {
        responseError: function(response) {
            if (response.status === 403) {
            	$location.path("/login");
            }
            return $q.reject(response);
        }
    };
    return errorResolverInterceptor;
  });

  app.config(['$httpProvider', function($httpProvider) {
    // Essa linha faz com que dados que vem json do servidor e transformados em um objeto tenham os tipos de dados de data convertidos para o objeto do tipo date.
    $httpProvider.defaults.transformResponse.push(function(responseData){
        convertDateStringsToDates(responseData);

        return responseData;
      });

    $httpProvider.interceptors.push('errorResolverInterceptor');
    $httpProvider.interceptors.push('accessDeniedInterceptor');
    
  }]);

})();
