/**
 * @ngdoc Controller
 * @name Login Controller
 * 
 * @description Controller para login
 * 
 * @author Edsf
 */
(function() {
	var app = angular.module('sge');

	app.controller('LoginController', function($state, authenticationService) {
		var vm = this;
		vm.usuario = {};

		this.doLogin = function() {
			authenticationService.doLogin(vm.usuario.login, vm.usuario.senha,
					function() {
						$state.go("home");
					}, function(resultado) {
						vm.error = "Login ou Senha Inválida";
					});
		};
	});

})();
