/**
 * @ngdoc controller
 * @name ClienteModalController
 * 
 * @description Controller para edição de um cliente.
 * 
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function() {
	var app = angular.module("sge");

	app.controller("ClienteModalController", function(clienteService,
			$uibModalInstance, growl, item) {
		var vm = this;
		vm.item = {};
		vm.editar = true;
		vm.descCodigo = "CPF";
		
		angular.copy(item, vm.item);
		if (vm.item.id == undefined || vm.item.id == null) {
			vm.editar = false;
			vm.titulo = "Novo Cliente";
		} else {
			vm.editar = true;
			vm.titulo = "Alterar Cliente";
		}

		vm.loading = false;

		vm.salvar = function() {
			vm.loading = true;

			if (!vm.editar) {
				clienteService.criar(vm.item, function(cliente) {
					growl.success("<b>Cliente</b> criado com sucesso", {
						disableCountDown : true
					});
					angular.copy(cliente, item);
					vm.loading = false;
					$uibModalInstance.close(item);
				}, function(validationErrors){
					var mensagem = "Falha na validação do cliente<br>";
					
					for(i = 0; i < validationErrors.length; i++) {
						mensagem += validationErrors[i]+"<br>";
					}
					
					growl.warning(mensagem, {
						disableCountDown : true
					});
					vm.loading = false;
				});
			} else {
				clienteService.alterar(vm.item, function(cliente) {
					growl.success("<b>Cliente</b> alterado com sucesso", {
						disableCountDown : true
					});
					angular.copy(cliente, item);
					vm.loading = false;
					$uibModalInstance.close(item);
				});
			}			
		}

		vm.cancelar = function() {
			$uibModalInstance.dismiss('cancel');
		}
		
		vm.mudarRadio = function(valor) {
			if(valor == 'F') {
				vm.descCodigo = "CPF";
			} else {
				vm.descCodigo = "CNPJ";
			}
		}
	});
})();
