/**
 * @ngdoc controller
 * @name AlunoController
 * 
 * @description Controller para consulta de alunos.
 * 
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function() {
	var app = angular.module("sge");

	app.controller("AlunoController", function(alunoService, $location,
			DTOptionsBuilder, DTColumnDefBuilder) {
		var vm = this;
		vm.alunos = [];

		vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType(
				'full_numbers').withDisplayLength(10).withBootstrap();

		vm.dtColumnDefs = [ DTColumnDefBuilder.newColumnDef(0).notSortable(),
				DTColumnDefBuilder.newColumnDef(1),
				DTColumnDefBuilder.newColumnDef(2) ];

		/**
		 * 
		 */
		vm.buscarTodos = function() {
			alunoService.buscarTodos(function(resultado) {
				vm.alunos = resultado;
			});
		}

		/**
		 * 
		 */
		vm.abrirAdmissoes = function(aluno) {
			$location.path("/home/admissao/" + aluno.matricula);
		}

		vm.buscarTodos();
	});
	
})();
