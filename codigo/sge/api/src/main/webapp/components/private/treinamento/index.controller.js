/**
 * @ngdoc Controller
 * @name TreinamentoController
 * 
 * @description Controller para paginas de consulta de treinamento
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function() { var app = angular.module('sge');

	app.controller('TreinamentoController', function(treinamentoService, $uibModal, DTOptionsBuilder, DTColumnDefBuilder, growl, $state, $location) {
		var vm = this;
		vm.treinamentos = [];
		vm.treinamento = {};

		vm.dtOptions = DTOptionsBuilder.newOptions()
		.withPaginationType('simple_numbers').withDisplayLength(10).withBootstrap();

		
		vm.buscarTreinamentos = function() {
			treinamentoService.getTreinamentos(function(resultado) {
				vm.treinamentos = resultado;
			});
			
		};

		vm.buscarTreinamentos();

		vm.adicionarItem = function() {
			$state.go("home.treinamentoEdit", {treinamentoEdit: null});
		};

		vm.editarItem = function(treinamento) {
			$state.go("home.treinamentoEdit", {treinamentoEdit: treinamento});
		};
		
		/**
		 * Remover um treinamento 
		 */
		vm.remover = function(treinamento) {
			var modalInstance = $uibModal
					.open({
						animation : true,
						templateUrl : "components/private/partials/dialog.confirm.html",
						size : 'sm',
						backdrop : false,
						controller : function($uibModalInstance, $scope) {
							$scope.sim = function() {
								$uibModalInstance.close(true);
							}

							$scope.cancelar = function() {
								$uibModalInstance.dismiss('cancel');
							}
						}
					});

			modalInstance.result.then(function(resultado) {
				if (resultado) {
					treinamentoService.remover(treinamento, function() {
								growl.success("<b>Treinamento</b> removido com sucesso", {disableCountDown: true});
								var index = vm.treinamentos.indexOf(treinamento);
								vm.treinamentos.splice(index, 1);
					});
				}
			});
		};
		
		
		vm.abrirDetalhes = function(treinamento) {
			$state.go("home.treinamentoDetail", {treinamentoDetail: JSON.stringify(treinamento)});
		}
		
	});
	
})();
