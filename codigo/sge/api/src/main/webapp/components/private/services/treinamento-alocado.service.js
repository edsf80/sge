/**
 * @ngdoc Services
 * @name treinamentoAlocadoService
 * 
 * @description requisições à endpoints de nível de um aluno
 * 
 * @author Edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function() {
	var app = angular.module("sge");

	app.factory("treinamentoAlocadoService", function($http) {
		var resultado = {};

		resultado.buscarPorAluno = function(matricula, callback) {
			$http.get("treinamentoAlocado/"+matricula).then(function(response) {
				callback(response.data);
			});
		};
		
		resultado.criar = function(treinamentoAlocado, callback) {
			$http.post("treinamentoAlocado", treinamentoAlocado).then(function(response) {
				callback(response.data);
			});
		};
		
	    resultado.alterar = function(treinamentoAlocado, callback) {
	      $http.put("treinamentoAlocado", treinamentoAlocado).then(function(response){
	        callback(response.data);
	      });
	    };

		resultado.apagar = function(treinamentoAlocado, callback) {
			$http.delete("treinamentoAlocado/"+treinamentoAlocado.id).then(function(){
				callback();
			})
		}
		
		return resultado;
	});
})();