angular.module('sge').directive('pageHeader', function() {
	return {
		templateUrl : 'components/private/partials/page-header.directive.html',
		restrict : 'E',
		replace : true,
	}
});