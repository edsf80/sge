/**
 * @ngdoc Services
 * @name nivelAlunoService
 * 
 * @description requisições à endpoints de nível de um aluno
 * 
 * @author Edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function() {
	var app = angular.module("sge");

	app.factory("nivelAlunoService", function($http) {
		var resultado = {};

		resultado.buscarPorAluno = function(matricula, callback) {
			$http.get("nivelaluno/"+matricula).then(function(response) {
				callback(response.data);
			});
		};
		
		resultado.buscarTodos = function(callback) {
			$http.get("nivelaluno/").then(function(response) {
				callback(response.data);
			});
		};
		
		resultado.buscarPorPrazo = function(prazo, callback) {
			var param = {
					params: {
						"inicio" : prazo.inicio,
						"fim" : prazo.fim
					}
			}
			$http.get("nivelaluno", param).then(function(response) {
				callback(response.data);
			});
		};	
		
		resultado.criar = function(nivelAluno, callback) {
			$http.post("nivelaluno", nivelAluno).then(function(response) {
				callback(response.data);
			});
		};

		resultado.apagar = function(nivelAluno, callback) {
			$http.delete("nivelaluno/"+nivelAluno.id).then(function(){
				callback();
			})
		}
		
		return resultado;
	});
})();