/**
 * @ngdoc Controller
 * @name TreinamentoDetailController
 * 
 * @description Controller para pagina de visualiação de treinamento
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function() {
	var app = angular.module('sge');

	app.controller('TreinamentoDetailController', function(treinamentoService,
			$stateParams, DTOptionsBuilder, DTColumnDefBuilder, $state) {

		var stateTreinamentoDetail = JSON.parse($stateParams.treinamentoDetail);

		var vm = this;
		vm.treinamento = {};
		vm.treinamentoAlocado = {};
		vm.materiais = [];
		vm.videos = [];
		

		vm.dtOptions = DTOptionsBuilder.newOptions()
				.withPaginationType('simple_numbers')
				.withDisplayLength(1)
				.withBootstrap()
				.withOption('responsive', true)
				.withOption('bFilter', false)
				.withOption('bInfo', false)
				.withOption('bSort', false)
				.withOption('lengthChange', false)
				.withPaginationType('full_numbers');

		vm.dtColumnDefs = [ DTColumnDefBuilder.newColumnDef(0).notSortable() ];

		/**
		 * 
		 */
		var verificarTipoConteudo = function() {
			if (vm.treinamento.conteudos.length) {
				vm.treinamento.conteudos.forEach(function(conteudo) {

					if (conteudo.tipo === 'DOCUMENTO') {
						vm.materiais.push(conteudo);
					}
					if (conteudo.tipo === 'VIDEO') {
						vm.videos.push(conteudo);
					}

				})
			}
		}
		
		if (stateTreinamentoDetail) {
			if(stateTreinamentoDetail.treinamento !== undefined){
				vm.treinamento = stateTreinamentoDetail.treinamento;
				vm.treinamentoAlocado = stateTreinamentoDetail;
			}else {
				vm.treinamento = stateTreinamentoDetail;
			}
			verificarTipoConteudo();
	}

		/**
		 * 
		 */
		vm.detailNivel = function() {
			if(stateTreinamentoDetail.treinamento !== undefined){
				$state.go('home.detalheProva', {
					detailProva : JSON.stringify(vm.treinamentoAlocado)
				});
			}else{
				$state.go('home.detalheProva', {
					detailProva : JSON.stringify(vm.treinamento.prova)
				});
			}
		};
		
		/**
		 * 
		 */
		vm.temMaterial = function() {
			if(vm.materiais && vm.materiais.length){
				return true;
			}
			return false;
		};
		
		/**
		 * 
		 */
		vm.temVideo = function() {
			if(vm.videos && vm.videos.length){
				return true;
			}
			return false;
		};
	});
})();
