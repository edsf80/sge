/**
 * @ngdoc controller
 * @name DemandaModalController
 * 
 * @description Controller para edição de uma demanda.
 * 
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	var app = angular.module("sge");

	app.controller("DemandaModalController", function(demandaService,
			$uibModalInstance, growl, item) {
		var vm = this;
		vm.item = {};
		vm.editar = true;
		vm.dtpk1 = false;
		vm.dtpk2 = false;		
		
		angular.copy(item, vm.item);
		if (vm.item.id == undefined || vm.item.id == null) {
			vm.editar = false;
			vm.titulo = "Nova Demanda";
			vm.item = {transicoes:['N']};
		} else {
			vm.editar = true;
			vm.titulo = "Alterar Demanda";
		}

		vm.loading = false;

		vm.salvar = function() {
			vm.loading = true;

			if (!vm.editar) {
				demandaService.criar(vm.item, function(demanda) {
					growl.success("<b>Demanda</b> criada com sucesso", {
						disableCountDown : true
					});
					angular.copy(demanda, item);
					vm.loading = false;
					$uibModalInstance.close(item);
				}, function(validationErrors){
					var mensagem = "Falha na validação da demanda<br>";
					
					for(i = 0; i < validationErrors.length; i++) {
						mensagem += validationErrors[i]+"<br>";
					}
					
					growl.warning(mensagem, {
						disableCountDown : true
					});
					vm.loading = false;
				});
			} else {
				demandaService.alterar(vm.item, function(demanda) {
					growl.success("<b>Demanda</b> alterada com sucesso", {
						disableCountDown : true
					});
					angular.copy(demanda, item);
					vm.loading = false;
					$uibModalInstance.close(item);
				});
			}			
		}

		vm.cancelar = function() {
			$uibModalInstance.dismiss('cancel');
		}

		vm.abrirData = function(datepicker) {
			if (datepicker == 1) {
				vm.dtpk1 = true;
			}

			if (datepicker == 2) {
				vm.dtpk2 = true;
			}
		}
	});
})();
