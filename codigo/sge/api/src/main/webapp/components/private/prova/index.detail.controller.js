/**
 * @ngdoc controller
 * @name Prova detalhe Controller
 * 
 * @description Controle para detalhes de uma prova.
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	var app = angular.module("sge");

	app.controller("ProvaDetailController", function(treinamentoAlocadoService, $uibModal, growl,
			$location, $stateParams, $state) {
		
		stateDetailProva = JSON.parse($stateParams.detailProva);
		
		var vm = this;
		vm.treinamentoAlocado = {}
		vm.prova = {};
		vm.questao = {};
		vm.respostaAtual = {};
		vm.respostas = [];
		vm.progresso = 0;
		
		vm.temResultado = false;
		vm.resultadoProva = {};

		/**
		 * 
		 */
		vm.proximaQuestao = function() {

			if(vm.frmOptions.$valid){
				vm.respostaAtual.questao = vm.questao;
				vm.respostas.push(vm.respostaAtual);
				proxima();
			  	vm.frmOptions.$setPristine();
			    vm.frmOptions.$setUntouched();
			    vm.respostaAtual = {};
			}
			
			if(vm.prova.questoes.length === cont) {
				if(vm.respostas.length){
					vm.temResultado = true;
					getResultado();
				}
			}

		};

		/**
		 * 
		 */
		var cont = 0;
		var proxima = function() {
			cont++;
			vm.progresso = cont;
			setQuestao(cont);
		};


		/**
		 * 
		 */
		var setQuestao = function(cont) {
			if (!(vm.prova.questoes.length === cont)) {
				vm.questao = vm.prova.questoes[cont];
			}
		};
		
		/**
		 * 
		 */
		if (stateDetailProva) {
			getTipoVisualizacao(stateDetailProva);
			vm.prova.questoes = gerarQuestoesAleatorias(vm.prova.questoes);
			setQuestao(cont);
		}
		
		/**
		 * 
		 */
		vm.refazerProva = function() {
			
			$state.go('home.detalheProva', {
				detailProva : JSON.stringify(stateDetailProva)
			});
			
			vm.questao = {};
			vm.respostaAtual = {};
			vm.respostas = [];
			vm.progresso = 0;
			vm.temResultado = false;
			vm.resultadoProva = {};
			vm.prova.questoes = gerarQuestoesAleatorias(vm.prova.questoes);
			cont = 0
			setQuestao(cont);
		}
		
		/**
		 * 
		 */
		vm.irParaTreinamentos = function() {
			if(isALunoRespondendo(stateDetailProva)){
				$state.go('home.treinamentoDetail', {
					treinamentoDetail : JSON.stringify(vm.treinamentoAlocado)
				});
				
			}else{
				$state.go('home.treinamento');
			}
		}
		
		/**
		 * 
		 */
		function getTipoVisualizacao(stateParam) {
			if(isALunoRespondendo(stateParam)){
				vm.prova = stateParam.treinamento.prova;
				vm.treinamentoAlocado = stateParam;				
			} else {
				vm.prova = stateParam;
			}
		}
		
		function isALunoRespondendo(stateParam) {
			return stateParam.treinamento != undefined; 
		}
		
		/**
		 * 
		 */
		vm.finalizada = function(){
			return vm.prova.questoes.length-1 === cont;
		};
		
		/**
		 * 
		 */
		var getResultado = function() {
			var pontos = 0
			var nQuestoes = vm.prova.questoes.length;
			var passou = false;
			
			vm.respostas.forEach(function(resposta) {
				if(resposta.alternativaMarcada.letra === resposta.questao.resposta) {
					pontos++;
				}
			});
			
			var percentual = (pontos / nQuestoes) * 100;
			var arredondado = parseFloat(percentual.toFixed(2));
			percentual = arredondado;
			
			if(percentual >= 100) {
				passou = true;
			}
			
			vm.resultadoProva.pontos = pontos;
			vm.resultadoProva.nQuestoes = nQuestoes;
			vm.resultadoProva.percentual = percentual;
			vm.resultadoProva.passou = passou;
			
			concluirTreinamento(passou);
			
			
		};
		
		/**
		 * 
		 */
		function gerarQuestoesAleatorias(questoes) {
			var questoesAleatorias = [];
			
			if(questoes.length > 9) {
				while (questoesAleatorias.length < 10) {
					var questao = questoes[Math.floor(Math.random()*questoes.length)];
					if(questoesAleatorias.indexOf(questao) === -1) {
						questoesAleatorias.push(questao);
					}
				}
			}else{
				questoesAleatorias = questoes;
			}
			
			return questoesAleatorias;
		}
		
		/**
		 * 
		 */
		var concluirTreinamento = function(aprovado) {
			if(aprovado && vm.treinamentoAlocado) {
				vm.treinamentoAlocado.finalizado = true;
				treinamentoAlocadoService.alterar(vm.treinamentoAlocado, function(){});
			}			
		}

	});

})();
