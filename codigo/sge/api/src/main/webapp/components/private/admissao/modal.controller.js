/**
 * @ngdoc controller
 * @name AdmissaoModalController
 * 
 * @description Controller para edição de uma admissão.
 * 
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function(){
  var app = angular.module("sge");

  app.controller("AdmissaoModalController", function(admissaoService, $uibModalInstance, growl, item){
    var vm = this;
    vm.item = {};
    vm.editar = true;
    vm.dtpk1 = false;
    vm.dtpk2 = false;

    angular.copy(item, vm.item);
    if(vm.item.dataInicio == undefined || vm.item.dataInicio == null) {
      vm.editar = false;
      vm.titulo = "Nova Admissão";
    } else {
      vm.editar = true;
      vm.titulo = "Alterar Admissão";
    }

    vm.loading = false;

    vm.salvar = function() {
      vm.loading = true;

      if(!vm.editar) {
        admissaoService.criar(vm.item, function(admissao){
          growl.success("<b>Admissão</b> criada com sucesso", {disableCountDown: true});
          angular.copy(admissao, item);
        });
      } else {
        admissaoService.alterar(vm.item, function(admissao){
          growl.success("<b>Admissão</b> alterada com sucesso", {disableCountDown: true});
          angular.copy(admissao, item);
        });
      }
      vm.loading = false;      
      $uibModalInstance.close(item);
    }

    vm.cancelar = function() {
      $uibModalInstance.dismiss('cancel');
    }

    vm.abrirData = function(datepicker) {
      if(datepicker == 1) {
        vm.dtpk1 = true;
      }

      if(datepicker == 2) {
        vm.dtpk2 = true;
      }
    }
  });
})();
