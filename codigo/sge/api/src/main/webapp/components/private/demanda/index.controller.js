/**
 * @ngdoc controller
 * @name DemandaController
 * 
 * @description Controller para consulta de demandas.
 * 
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	var app = angular.module('sge');

	app.controller('DemandaController', function(demandaService, $uibModal,
			DTOptionsBuilder, DTColumnDefBuilder) {
		var vm = this;
		vm.demandas = [];

		vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType(
				'full_numbers').withDisplayLength(10).withBootstrap();
		vm.dtColumnDefs = [ DTColumnDefBuilder.newColumnDef(0).notSortable().withOption('width', '8%'),
				DTColumnDefBuilder.newColumnDef(1).withOption('width', '8%'),
				DTColumnDefBuilder.newColumnDef(2),
				DTColumnDefBuilder.newColumnDef(3) ];

		vm.buscarDemandas = function() {
			demandaService.getDemandas(function(resultado) {
				vm.demandas = resultado;
			});
		}

		vm.buscarDemandas();

		function abrirModal(demanda) {
			return $uibModal.open({
				templateUrl : 'components/private/demanda/modal.view.html',
				controller : 'DemandaModalController',
				controllerAs : 'demandaMCtrl',
				backdrop : 'static',
				resolve : {
					item : function() {
						return demanda;
					}
				}
			});
		}

		vm.adicionarItem = function() {
			var modal = abrirModal({});

			modal.result.then(function(item) {
				vm.demandas.push(item);
			});
		}

		vm.editarItem = function(demanda) {
			abrirModal(demanda);
		}
	});

	app.filter('status', function() {
		return function(input) {
			input = input || '';
			var out = '';

			switch (input) {
			case 'A':
				out = 'Aprovada';
				break;
			case 'L':
				out = 'Em Análise';
				break;
			case 'F':
				out = 'Finalizada';
				break;
			case 'E':
				out = 'Em Andamento';
				break;
			case 'N':
				out = 'Nova';
				break;
			case 'C':
				out = 'Cancelada';
				break;
			}

			return out;
		};
	});

})();
