/**
 * @ngdoc Services
 * @name nivelService
 * 
 * @description requisições à endpoints de um nível
 * 
 * @author Edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function(){
  var app = angular.module("sge");

  app.factory("nivelService", function($http){
    var resultado = {};

    resultado.buscarTodosNiveis = function(callback) {
      $http.get("nivel").then(function(response){
        callback(response.data);
      });
    }
    
    resultado.criar = function(nivel, callback) {
      $http.post("nivel", nivel).then(function(response){
        callback(response.data);
      });
    }

    resultado.alterar = function(nivel, callback) {
      $http.put("nivel", nivel).then(function(response){
        callback(response.data);
      });
    }

    resultado.excluir = function(nivel, callback) {
      $http.delete("nivel").then(function(response){
        callback(response.data);
      });
    }

    return resultado;
  });
})();
