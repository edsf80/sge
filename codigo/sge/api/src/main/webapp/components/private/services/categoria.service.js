/**
 * @ngdoc Services
 * @name categoriaService
 * 
 * @description endpoints de categoria
 * 
 * @author Edsf
 */
(function() {
  angular.module('sge').factory('categoriaService', function($http){
    var resultado = {};

    resultado.getCategorias = function(callback) {
      $http.get('/api/categoria')
        .success(function (response) {
          callback(response.dados);
        });
    };

    resultado.criar = function(categoria, callback) {
      $http.post('/api/categoria', categoria)
        .success(function(response){
          callback(response.dados);
        });
    };

    resultado.alterar = function(categoria, callback) {
      $http.put('/api/categoria', categoria)
        .success(function(response){
          callback(response.dados);
        })
    }

    return resultado;
  });

})();
