angular.module('sge')
		.directive(
				'headerNotification',
				function() {
					return {
						templateUrl : 'components/private/partials/header-notification.directive.html',
						restrict : 'E',
						replace : true,
						controller: function($rootScope, $location, authenticationService, nivelAlunoService) {
							
							var vm = this;
							vm.niveis = [];
							vm.atrasados = [];

							/**
							 * 
							 */
							vm.logout = function() {
								authenticationService.doLogout(function() {									
									$location.path("/login");
								});
							}
							
							vm.usuario = $rootScope.sessionUser;
							
							/**
							 * 
							 */
							vm.getNiveisAtrasos = function() {
								var hoje = new Date();
		
								nivelAlunoService.buscarTodos(function(response) {
									vm.niveis = response;
									var dataAtual = new Date().getTime();
							
									vm.niveis.forEach(function(nivelAluno) {
										var dataNivel = new Date(nivelAluno.prazoConclusao).getTime();
										console.log(nivelAluno)
										if(dataNivel < dataAtual && nivelAluno.treinamentos.length && !nivelAluno.concluido) {
											vm.atrasados.push(nivelAluno);
										}
									})
								})
							}
							
							vm.getNiveisAtrasos();
							
							/**
							 * 
							 */
							vm.temNiveisAtrasados = function() {
								return vm.atrasados.length > 0;
							}
							
							/**
							 * 
							 */
							vm.abrirAdmissoes = function(matricula) {
								$location.path("/home/admissao/" + matricula);
							}
							
							
						},
						controllerAs: 'headerNotificationCtrl'
					}
				});
