/**
 * @ngdoc Services
 * @name alunoService
 * 
 * @description Serviços de endpoints de alunos
 * 
 * @author Edsf
 */

(function() {
  angular.module("sge").factory("alunoService", function($http){
    var resultado = {};

    resultado.buscarTodos = function(callback) {
      $http.get("aluno").then(function(response){
        callback(response.data);
      });
    };

    resultado.buscarPorMatricula = function(matricula, callback) {
      $http.get("aluno/"+matricula).then(function(response){
        callback(response.data);
      });
    }

    return resultado;
  });
})();
