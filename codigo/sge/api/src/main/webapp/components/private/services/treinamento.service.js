
/**
 * @ngdoc Services
 * @name treinamentoService
 * 
 * @description requisições à endpoints de treinamentos
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function(){
  angular.module("sge").factory("treinamentoService", function($http){
    var resultado = {};

    /**
     * Busca todos os treinamentos na API
     */
	resultado.getTreinamentos = function(callback) {
		$http.get("treinamento").then(function(response) {
			callback(response.data);
		});
	};

	/**
	 * Salva um treinamento na API
	 */
    resultado.salvar = function(treinamento, callback){
      $http.post("treinamento", treinamento).then(function(response){
        callback(response.data);
      });
    };

    /**
     * Altera um treinamento na API
     */
    resultado.alterar = function(treinamento, callback) {
      $http.put("treinamento", treinamento).then(function(response){
        callback(response.data);
      });
    };
    
    /**
     * Remove um treinamento na API
     */
    resultado.remover = function(treinamento, callback) {
        $http.delete("treinamento/" + treinamento.id).then(function(response){
          callback(response.data);
        });
      };
        
        /**
         * Busca um treinamento por nível
         */
        resultado.getPorNivel = function(idNivel, callback) {
            $http.get("treinamento/"+idNivel).then(function(response){
              callback(response.data);
            });
          };

    return resultado;
    
  });
  
})();
