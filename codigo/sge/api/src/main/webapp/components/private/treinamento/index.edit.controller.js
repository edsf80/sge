/**
 * @ngdoc Controller
 * @name TreinamentoEditController
 * 
 * @description Controller para pagina de edição de treinamento
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	var app = angular.module("sge");

	app.controller("TreinamentoEditController", function(treinamentoService, nivelService,
			provaService, growl, $stateParams, $state) {
		var vm = this;

		vm.item = {};
		vm.conteudo = {};
		vm.provas = {};
		vm.niveis = [];
		vm.temConteudo = true;

		if ($stateParams.treinamentoEdit) {
			vm.item = $stateParams.treinamentoEdit;
		}

		/**
		 * Editar/Salvar um treinamento
		 */
		vm.salvar = function() {
			if (temConteudo(vm.item.conteudos)) {
				if (!vm.isEdited()) {
					vm.item.finalizado = false;
					treinamentoService.salvar(vm.item, function(response) {
						growl.success("<b>Treinamento</b> criado com sucesso",
								{
									disableCountDown : true
								});
						$state.go('home.treinamento');
					});

				} else {
					treinamentoService.alterar(vm.item, function(response) {
						growl.success(
								"<b>Treinamento</b> alterado com sucesso", {
									disableCountDown : true
								});
						$state.go('home.treinamento');
					});
				}
				;
			} else {
				vm.temConteudo = false;
			}
		};

		vm.cancelar = function() {
			$state.go('home.treinamento');
		};

		/**
		 * Adiciona um novo conteudo na tabela de coteúdos
		 */
		vm.adicionarConteudo = function() {
			if (!vm.item.conteudos) {
				vm.item.conteudos = [];
			}
			vm.item.conteudos.push(vm.conteudo);
			vm.conteudo = angular.copy(vm.conteudo = {});
			vm.temConteudo = true;
		};

		/**
		 * Remove um item da tabela de conteudos
		 */
		vm.removeConteudo = function(conteudo) {
			var index = vm.item.conteudos.indexOf(conteudo);
			vm.item.conteudos.splice(index, 1);
		};
		

		/**
		 * verifica se é edição ou cadastro de um treinamento
		 */
		vm.isEdited = function() {
			return vm.item.id;
		};

		/**
		 * verifica se possui conteudo na lista de conteudos de um treinamento
		 */
		var temConteudo = function(conteudos) {
			return conteudos && conteudos.length > 0;
		};

		/**
		 * 
		 */
		vm.getProvas = function() {
			provaService.getProvas(function(resultado) {
				vm.provas = resultado;
			});
		};
		
		/**
		 * 
		 */
		vm.getNiveis = function() {
			nivelService.buscarTodosNiveis(function(resultado) {
				vm.niveis = resultado;
			});
		}

		vm.getNiveis();
		vm.getProvas();

	});

})();
