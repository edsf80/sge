/**
 * @ngdoc controller
 * @name Prova Controller
 * 
 * @description Controle para consulta de prova.
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() { var app = angular.module('sge');

	app.controller('provaController', function(provaService, DTOptionsBuilder, DTColumnDefBuilder, growl, $state, $location, $uibModal) {
		var vm = this;
		vm.provas = [];
		vm.prova = {};

		vm.dtOptions = DTOptionsBuilder.newOptions()
		.withPaginationType('simple_numbers').withDisplayLength(10).withBootstrap();

		
		vm.buscarProvas = function() {
			provaService.getProvas(function(resultado) {
				vm.provas = resultado;
			});
			
		};

		vm.buscarProvas();

		/**
		 * Remover uma prova 
		 */
		vm.remover = function(prova) {
			var modalInstance = $uibModal
					.open({
						animation : true,
						templateUrl : "components/private/partials/dialog.confirm.html",
						size : 'sm',
						backdrop : false,
						controller : function($uibModalInstance, $scope) {
							$scope.sim = function() {
								$uibModalInstance.close(true);
							}

							$scope.cancelar = function() {
								$uibModalInstance.dismiss('cancel');
							}
						}
					});

			modalInstance.result.then(function(resultado) {
				if (resultado) {
					provaService.remover(prova, function() {
								growl.success("<b>Prova</b> removida com sucesso", {disableCountDown: true});
								var index = vm.provas.indexOf(prova);
								vm.provas.splice(index, 1);
					});
				}
			});
		};
		
		/**
		 * 
		 */
		vm.novaProva = function() {
			$state.go('home.provaEdit', {provaEdit: null});
		}
		
		/**
		 * 
		 */
		vm.editarProva = function(prova) {
			$state.go('home.provaEdit', {provaEdit: prova});
		};
		
		vm.abrirDetalhes = function(prova) {
			$state.go('home.detalheProva', {
				detailProva : JSON.stringify(prova)
			});
		};
		
	});
	
})();
