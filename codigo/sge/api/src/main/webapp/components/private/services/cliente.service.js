/**
 * @ngdoc Services
 * @name clienteService
 * 
 * @description endpoints de clientes
 * 
 * @author Edsf
 */
(function() {
	angular.module("sge").factory("clienteService", function($http) {
		var resultado = {};

		resultado.getClientes = function(callback) {
			$http.get("cliente").then(function(response) {
				callback(response.data);
			});
		};

		resultado.criar = function(item, callback, callbackValidation) {
			$http.post("cliente", item).then(function(response) {
				callback(response.data);
			}, function(error) {
				if(error.status == 422) {
					callbackValidation(error.data)
				}
			});
		};

		resultado.alterar = function(item, callback) {
			$http.put("cliente", item).then(function(response) {
				callback(response.data);
			});
		};

		return resultado;
	});
})();
