/**
 * @ngdoc controller
 * @name AdmissaoController
 * 
 * @description Controller para consulta de admissões.
 * 
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function() {
	var app = angular.module("sge");

	app
			.controller(
					"AdmissaoController",
					function(alunoService, nivelService, nivelAlunoService, treinamentoService, $stateParams, $uibModal,
							growl) {
						var vm = this;
						vm.aluno = {};
						vm.exibirAdicao = true;
						vm.niveis = [];
						vm.nivelSelecionado = {};

						vm.buscarTodosNiveis = function() {
							nivelService.buscarTodosNiveis(function(resultado) {
								vm.niveis = resultado;
							});
						}

						vm.buscarPorMatricula = function(matricula) {
							alunoService.buscarPorMatricula(matricula,
									function(resultado) {
										vm.aluno = resultado;
										if (vm.aluno.niveis !== undefined) {
											vm.aluno.niveis.forEach(function(nivelAluno, indexAluno) {
												removerNivel(nivelAluno.nivel, vm.niveis);
											});
										}
										exibirAdicao();
									});
						};

						vm.adicionarNivel = function() {
							treinamentoService.getPorNivel(vm.nivelSelecionado.id, function(resposta) {
								
								var nivelAlunoSelecionado = {
										nivel : vm.nivelSelecionado,
										matricula : vm.aluno.matricula,
										treinamentos: setTreinamentosAlocados(resposta),
										prazoConclusao : setPrazoConclusao(resposta)
								}
								
								nivelAlunoService.criar(nivelAlunoSelecionado, function(resposta) {
									vm.aluno.niveis.push(resposta);
									removerNivel(resposta.nivel, vm.niveis);
									growl.success("<b>Nível</b> adicionado com sucesso",{
										disableCountDown : true
									});
								});
							})
						}
						
						function setTreinamentosAlocados(treinamentos) {
							var treinamentosAlocados = [];
							treinamentos.forEach(function(treinamento) {
								var treinamentoAlocado = {
									finalizado : false,
									matricula : vm.aluno.matricula,
									treinamento : treinamento
								};
								treinamentosAlocados.push(treinamentoAlocado);
							});
							return treinamentosAlocados;
						}

						/**
						 * 
						 */
						function setPrazoConclusao(treinamentos) {
							var diasTreinamentos = 0;
							treinamentos.forEach(function(treinamento) {
								diasTreinamentos += treinamento.tempoTreinamento;
							})
							var hoje = new Date();
							var dataPrazo = new Date();
							dataPrazo.setDate(hoje.getDate() + diasTreinamentos);

							return dataPrazo;
						}

						function exibirAdicao() {
							for (i = 0; i < vm.aluno.admissoes.length; i++) {
								if (vm.aluno.admissoes[i].dataTermino === undefined) {
									vm.exibirAdicao = false;
								}
							}
						}

						function removerNivel(nivel, lista) {
							lista.forEach(function(item, index) {
								if (nivel.id == item.id) {
									lista.splice(index, 1);
								}
							})
						}

						vm.buscarTodosNiveis();
						vm.buscarPorMatricula($stateParams.matricula);

						function abrirModal(admissao) {
							return $uibModal.open({
								templateUrl : 'components/private/admissao/modal.view.html',
								controller : 'AdmissaoModalController',
								controllerAs : 'admissaoMCtrl',
								backdrop : 'static',
								resolve : {
									item : function() {
										return admissao;
									}
								}
							});
						}

						vm.adicionarItem = function() {
							var modal = abrirModal({
								matricula : vm.aluno.matricula
							});

							modal.result.then(function(item) {
								vm.aluno.admissoes.push(item);
								exibirAdicao();
							});
						}

						vm.editarItem = function(admissao) {
							abrirModal(admissao);
						}

						vm.excluirNivel = function(nivelAluno) {
							var modalInstance = $uibModal
									.open({
										animation : true,
										template : "<div class='modal-header'><h3 class='modal-title' id='modal-title'>Confirmação!</h3></div><div class='modal-body' id='modal-body'>Deseja Realmente apagar o registro?</div><div class='modal-footer'><button class='btn btn-primary' ng-click='sim()' type='button'>Sim</button> <button class='btn btn-warning' ng-click='cancelar()' type='button'>Não</button></div>",
										size : 'sm',
										backdrop : false,
										controller : function(
												$uibModalInstance, $scope) {
											$scope.sim = function() {
												$uibModalInstance.close(true);
											}

											$scope.cancelar = function() {
												$uibModalInstance
														.dismiss('cancel');
											}
										}
									});

							modalInstance.result
									.then(function(resultado) {
										if (resultado) {
											nivelAlunoService
													.apagar(
															nivelAluno,
															function() {
																vm.niveis
																		.push(nivelAluno.nivel);
																removerNivel(
																		nivelAluno,
																		vm.aluno.niveis);
																growl
																		.success(
																				"<b>Nível</b> removido com sucesso",
																				{
																					disableCountDown : true
																				});
															});
										}
									});
						};
					});
})();
