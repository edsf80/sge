/**
 * @ngdoc Services
 * @name demandaService
 * 
 * @description requisições à endpoints de demandas
 * 
 * @author Edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function() {
	angular.module("sge").factory("demandaService", function($http) {
		var resultado = {};

		resultado.getDemandas = function(callback) {
			$http.get("demanda").then(function(response) {
				callback(response.data);
			});
		};

		resultado.criar = function(demanda, callback, callbackValidation) {
			$http.post("demanda", demanda).then(function(response) {
				callback(response.data);
			}, function(error) {
				if(error.status == 422) {
					callbackValidation(error.data)
				}
			});
		};

		resultado.alterar = function(demanda, callback) {
			$http.put("demanda", demanda).then(function(response) {
				callback(response.data);
			});
		};

		return resultado;
	});
})();
