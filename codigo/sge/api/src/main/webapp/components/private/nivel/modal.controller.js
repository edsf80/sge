/**
 * @ngdoc controller
 * @name NivelModalController
 * 
 * @description Controller para edição de um nível.
 * 
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	var app = angular.module("sge");

	app.controller("NivelModalController", function(nivelService, $uibModalInstance, growl, item) {

		var vm = this;
		vm.item = {};
		vm.editar = true;
		angular.copy(item, vm.item);

		if (vm.item.id == undefined || vm.item.id == null) {
			vm.editar = false;
			vm.titulo = "Novo Nível";
		} else {
			vm.editar = true;
			vm.titulo = "Alterar Nível";
		}

		vm.loading = false;

		vm.salvar = function() {

				vm.loading = true;
				if (!vm.editar) {
					nivelService.criar(vm.item, function(nivel) {
						growl.success("<b>Nível</b> criado com sucesso", {
							disableCountDown : true
						});
						angular.copy(nivel, item);
						vm.loading = false;
						$uibModalInstance.close(item);

					}, function(validationErrors) {
						var mensagem = "Falha na validação do nível<br>";

						for (i = 0; i < validationErrors.length; i++) {
							mensagem += validationErrors[i] + "<br>";
						}

						growl.warning(mensagem, {
							disableCountDown : true
						});
						vm.loading = false;
					});
				} else {
					nivelService.alterar(vm.item, function(nivel) {
						growl.success("<b>Nível</b> alterado com sucesso", {
							disableCountDown : true
						});
						angular.copy(nivel, item);
						vm.loading = false;
						$uibModalInstance.close(item);
					});
				}
		};

		/**
		 * 
		 */
		vm.cancelar = function() {
			$uibModalInstance.dismiss('cancel');
		};
	});
})();
