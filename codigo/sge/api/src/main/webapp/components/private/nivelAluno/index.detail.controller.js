/**
 * @ngdoc controller
 * @name NivelAlunoDetailController
 * 
 * @description Controller para detalhes de nível de aluno
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	var app = angular.module('sge');

	app.controller('NivelAlunoDetailController', function(treinamentoService, $stateParams, $state) {

		var vm = this;
		vm.nivelAluno = {};

		if($stateParams.detailNivelAluno){
			vm.nivelAluno = JSON.parse($stateParams.detailNivelAluno);
		}
		
		vm.abrirDetalhesTreinamento = function(treinamentoAlocado) {
			$state.go("home.treinamentoDetail", {treinamentoDetail: JSON.stringify(treinamentoAlocado)});
		}
		
		vm.abrirDetalhesProva = function(treinamentoAlocado) {
			$state.go('home.detalheProva', {
				detailProva : JSON.stringify(treinamentoAlocado)
			});
		};
		
	});
})();
