/**
 * @ngdoc controller
 * @name 
 * ProvaEditController
 * 
 * @description 
 * Controle para manipular prova.
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	var app = angular.module("sge");

	app.controller("ProvaEditController", function(provaService, $uibModal, growl,$location, $stateParams) {
		var vm = this;
		vm.prova = {};

		if($stateParams.provaEdit){
			vm.prova = $stateParams.provaEdit;
		}
	
	/**
	 * Editar/Salvar uma prova
	 */
	vm.salvar = function() {

		vm.loading = true;

		if(vm.prova.questoes.length){
			if (!vm.isEdited()) {
				provaService.salvar(vm.prova, function(response) {
					growl.success("<b>Prova</b> criada com sucesso", {
						disableCountDown : true
					});
					vm.loading = false;
					$location.path("/home/prova");
				});
	
			} else {
				provaService.alterar(vm.prova, function(response) {
					growl.success("<b>Prova</b> alterada com sucesso", {
						disableCountDown : true
					});
					vm.loading = false;
					$location.path("/home/prova");
				});
			};
		}
	};
	
	/**
	 * 
	 */
	function abrirModalQuestao(questao) {
		return $uibModal.open({
			templateUrl : 'components/private/prova/modal.view.html',
			controller : 'QuestaoModalController',
			controllerAs : 'questaoMCtrl',
			backdrop : 'static',
			resolve : {
				item : function() {
					return questao;
				}
			}
		});
	};

	/**
	 * 
	 */
	vm.adicionarItem = function() {
		var modal = abrirModalQuestao({});

		modal.result.then(function(item) {
			
			if(!vm.prova.questoes){
				vm.prova.questoes = [];
			}
			vm.prova.questoes.push(item);
		});
	};
	
	/**
	 * Remove uma questao da tabela de questões
	 */
	vm.removerQuestao = function(questao){
		var index = vm.prova.questoes.indexOf(questao);
		vm.prova.questoes.splice(index, 1);
	};

	/**
	 * 
	 */
	vm.editarItem = function(item) {
		var modal = abrirModalQuestao(item);
		
		modal.result.then(function(response) {
			var index = vm.prova.questoes.indexOf(item);
			vm.prova.questoes.splice(index, 1);
			vm.prova.questoes.push(response);
		});
	};

	/**
	 * Fecha modal
	 */
	vm.cancelar = function() {
		$location.path("/home/prova");
	};

	/**
	 * verifica se é edição ou cadastro de uma Prova
	 */
	vm.isEdited = function() {
		return vm.prova.id;
	};
});

})();
