/**
 * @ngdoc controller
 * @name ClienteController
 * 
 * @description Controller para consulta de clientes.
 * 
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	var app = angular.module('sge');

	app.controller('ClienteController', function(clienteService, $uibModal,
			DTOptionsBuilder, DTColumnDefBuilder) {
		var vm = this;
		vm.clientes = [];

		vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType(
				'full_numbers').withDisplayLength(10).withBootstrap();
		vm.dtColumnDefs = [ DTColumnDefBuilder.newColumnDef(0).notSortable().withOption('width', '8%'),
				DTColumnDefBuilder.newColumnDef(1),
				DTColumnDefBuilder.newColumnDef(2),
				DTColumnDefBuilder.newColumnDef(3) ];

		vm.buscarClientes = function() {
			clienteService.getClientes(function(resultado) {
				vm.clientes = resultado;
			});
		}

		vm.buscarClientes();

		function abrirModal(cliente) {
			return $uibModal.open({
				templateUrl : 'components/private/cliente/modal.view.html',
				controller : 'ClienteModalController',
				controllerAs : 'clienteMCtrl',
				backdrop : 'static',
				resolve : {
					item : function() {
						return cliente;
					}
				}
			});
		}

		vm.adicionarItem = function() {
			var modal = abrirModal({});

			modal.result.then(function(item) {
				vm.clientes.push(item);
			});
		}

		vm.editarItem = function(item) {
			abrirModal(item);
		}
	});
	
	app.filter('tipoPessoa', function() {
		return function(input) {
			input = input || '';
			var out = '';

			switch (input) {
			case 'F':
				out = 'Física';
				break;
			case 'J':
				out = 'Jurídica';
				break;
			}

			return out;
		};
	});
})();
