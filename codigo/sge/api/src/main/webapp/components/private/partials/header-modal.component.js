/**
 * @ngdoc component
 * @name Header de modal
 * 
 * @description Componente para header de modal
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	angular.module("sge").component('headerModal', {

		bindings : {
			label : '<',
			close: '&'
		},

		templateUrl : 'components/private/partials/header-modal.component.html'

	});
})();