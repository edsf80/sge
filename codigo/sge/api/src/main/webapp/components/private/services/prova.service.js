/**
 * @ngdoc Services
 * @name provaService
 * 
 * @description requisições à endpoints de provas
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function(){
  angular.module("sge").factory("provaService", function($http){
    var resultado = {};

    /**
     * Busca todas as provas na API
     */
	resultado.getProvas = function(callback) {
		$http.get("prova").then(function(response) {
			callback(response.data);
		});
	};

	/**
	 * Salva uma prova na API
	 */
    resultado.salvar = function(prova, callback){
      $http.post("prova", prova).then(function(response){
        callback(response.data);
      });
    };

    /**
     * Altera uma prova na API
     */
    resultado.alterar = function(prova, callback) {
      $http.put("prova", prova).then(function(response){
        callback(response.data);
      });
    };
    
    /**
     * Remove uma prova na API
     */
    resultado.remover = function(prova, callback) {
        $http.delete("prova/" + prova.id).then(function(response){
          callback(response.data);
        });
      };
      
      /**
       * Busca uma prova pelo id
       */
      resultado.getById = function(id, callback) {
          $http.get("prova/"+id).then(function(response){
            callback(response.data);
          });
        };
        
    return resultado;
    
  });
  
})();
