angular.module('sge').directive('header', function() {
	return {
		templateUrl : 'components/private/partials/header.directive.html',
		restrict : 'E',
		replace : true,
	}
});