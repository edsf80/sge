/**
 * @ngdoc component
 * @name Help block
 * 
 * @description Componente mensagens de erros customizadas
 * 				em elementos fora de form.
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	angular.module("sge").component('helpBlock', {

		bindings : {
			message : '<',
			show: '='
		},

		template : '<div ng-show="$ctrl.show" class="sge-txt-danger">{{$ctrl.message}}</div>'

	});
})();