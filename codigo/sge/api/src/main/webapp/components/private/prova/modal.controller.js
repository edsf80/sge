/**
 * @ngdoc controller
 * @name Questão Modal Controller
 * 
 * @description Controle para manipular modal de edição de uma prova.
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function() {
	var app = angular.module("sge");

	app.controller("QuestaoModalController", function($uibModalInstance, growl, item, sgeValue) {
		var vm = this;
		vm.item = {};
		angular.copy(item, vm.item);
		vm.isEdited = isEdited();
		
		if (!isEdited()) {
			vm.item.alternativas = alternativas();
			
		}
		
		/**
		 * Função verifica a resposta referente a alternativa
		 * e marca um botão como checado.
		 */
		vm.setResposta = function(letra) {
			vm.item.resposta = letra;
			
			var alternativa = {};
			vm.item.alternativas.forEach(function(alt){
				
				if(alt.letra === letra){
					alternativa = alt;
				}
		});
	
		alteraLabelResposta(alternativa);
	};
	

	/**
	 * Editar/Salvar uma questão
	 */
	vm.salvar = function() {

		vm.loading = true;
		
		if (vm.item.enunciado && vm.item.resposta && vm.validaAlternativas()) {
			$uibModalInstance.close(vm.item);
			
		}
		vm.loading = false;
	};
	
	/**
	 * Fecha modal
	 */
	vm.cancelar = function() {
		$uibModalInstance.dismiss('cancel');
	};
	
	/**
	 * verifica se é edição ou cadastro de uma questão
	 */
	function isEdited () {
		return vm.item.alternativas;
	};
	
	/**
	 * 
	 */
	var alteraLabelResposta = function(alternativa){
		var index = vm.item.alternativas.indexOf(alternativa);
		var btn = $('.js-alt').eq(index);
		$('.js-alt').removeClass('btn-success').addClass('btn-danger');
		$('.js-alt').text('INCORRETA');

		if (btn.hasClass("btn-danger")) {
			btn.removeClass('btn-danger').addClass('btn-success');
			btn.text('CORRETA');
		}
	};
	
	
	/**
	 * Funcção utilária para verificar
	 * se alternativas foram preenchidas.
	 */
	
	vm.validaAlternativas = function() {
		var flag = 1;
		vm.item.alternativas.forEach(function(alt){
			if(alt.descricao === sgeValue.prova.descricao || isEmpty(alt.descricao)){
				flag += 1;
			}
		});
			
		if(flag > 1){
			return false;
		}else{
			return true;
		}

	};

	/**
	 * Função utilitária para verificar string
	 */
	function isEmpty(str) {
	    return (!str || 0 === str.length);
	};
	
	function alternativas(){
		descricao = sgeValue.prova.descricao;

		alts = [ {
			descricao : descricao,
			letra : 'A'
		}, {
			descricao : descricao,
			letra : 'B'
		}, {
			descricao : descricao,
			letra : 'C'
		}, {
			descricao : descricao,
			letra : 'D'
		}, {
			descricao : descricao,
			letra : 'E'
		} ]
		
		return alts;
	}
	
});

})();
