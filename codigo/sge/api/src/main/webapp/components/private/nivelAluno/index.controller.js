/**
 * @ngdoc controller
 * @name NivelAlunoController
 * 
 * @description Controle para consulta de níveis individual por aluno.
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	var app = angular.module('sge');

	app.controller('NivelAlunoController', function(alunoService,
			DTOptionsBuilder, DTColumnDefBuilder, $rootScope, $state) {
		
		var vm = this;
		vm.niveisAluno = [];

		vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType(
				'full_numbers').withDisplayLength(10).withBootstrap();
		vm.dtColumnDefs = [ DTColumnDefBuilder.newColumnDef(0).notSortable(),
				DTColumnDefBuilder.newColumnDef(2) ];

		/**
		 * 
		 */
		vm.buscarNiveisAlunos = function() {
			buscarAlunoPorMatricula($rootScope.sessionUser.matricula);
		}

		vm.buscarNiveisAlunos();

		/**
		 * 																																																																																																																														
		 */
		function buscarAlunoPorMatricula(matricula) {
			alunoService.buscarPorMatricula(matricula, function(resultado) {
				if (resultado.niveis !== undefined) {
					vm.niveisAluno = resultado.niveis;
				}
			});
		}
																						
		/**																																		
		 * 
		 */
		vm.detailNivel = function(nivelAluno) {
			$state.go('home.detalheNivelAluno', {
				detailNivelAluno : JSON.stringify(nivelAluno)
			});
		};
	});
})();
