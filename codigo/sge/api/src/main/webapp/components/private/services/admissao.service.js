/**
 * @ngdoc Services
 * @name admissaoService
 * 
 * @description Services de admissões
 * 
 * @author Edsf
 */
(function(){
  angular.module("sge").factory("admissaoService", function($http){
    var resultado = {};

    resultado.buscarPorIdColaborador = function(id, callback) {
      $http.get("/api/admissao/colaborador/"+id).success(function(response){
        callback(response.dados);
      });
    };

    resultado.criar = function(admissao, callback){
      $http.post("admissao", admissao).then(function(response){
        callback(response.data);
      });
    };

    resultado.alterar = function(admissao, callback) {
      $http.put("admissao", admissao).then(function(response){
        callback(response.data);
      });
    };

    return resultado;
  });
})();
