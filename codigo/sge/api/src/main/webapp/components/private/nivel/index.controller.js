/**
 * @ngdoc controller
 * @name NivelController
 * 
 * @description Controle para consulta de níveis.
 * 
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	var app = angular.module('sge');

	app.controller('NivelController', function(nivelService,
			DTOptionsBuilder, DTColumnDefBuilder, $uibModal) {
		var vm = this;
		vm.niveis = [];

		vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType(
				'full_numbers').withDisplayLength(10).withBootstrap();
		vm.dtColumnDefs = [ DTColumnDefBuilder.newColumnDef(0).notSortable(),
				DTColumnDefBuilder.newColumnDef(2) ];

		/**
		 * 
		 */
		vm.buscarNiveis = function() {
			nivelService.buscarTodosNiveis(function(resultado) {
				vm.niveis = resultado;
			});
		}

		vm.buscarNiveis();

		function abrirModal(nivel) {
			return $uibModal.open({
				templateUrl : 'components/private/nivel/modal.view.html',
				controller : 'NivelModalController',
				controllerAs : 'nivelMCtrl',
				backdrop : 'static',
				resolve : {
					item : function() {
						return nivel;
					}
				}
			});
		}

		vm.adicionarItem = function() {
			var modal = abrirModal({});

			modal.result.then(function(item) {
				vm.niveis.push(item);
			});
		}

		vm.editarItem = function(nivel) {
			abrirModal(nivel);
		}
	});
})();
