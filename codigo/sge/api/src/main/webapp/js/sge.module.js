/**
 * @ngdoc Module
 * @name sge
 * 
 * @description módulo principal da aplicação
 * 
 * @author Edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
    var app = angular.module('sge', ['sgeRoute', 'ui.bootstrap',
        /*'ngMockE2E'*/, 'angular-growl', 'angular-loading-bar', 'ngAnimate', 'datatables',
        'datatables.bootstrap', 'hyperactive.angular.utils', 'youtube-embed', 'ngCookies'
    ]);

    app.run([
        '$rootScope',
        '$location',
        '$http',
        'authenticationService',
        function($rootScope, $location, $http, authenticationService) {
        	
        	$rootScope.$on('$stateChangeSuccess', function(event, to, toParams, from, fromParams) {
                $rootScope.previousState = from;
            });

            $rootScope.$on('$locationChangeStart', function(event, next, current) {
                authenticationService.usuarioLogado(function(logado) {

                    if (logado) {
                        if ($location.path() === '/login') {
                            $location.path('/home/dashboard');
                        }
                        
                        if(next == current) {
                            event.preventDefault();
                            $location.path('/home');
                        }
              
                    } else {
                        if ($location.path() !== '/login') {
                            $location.path('/login');
                        }
                    }
                });
            });
        }
    ]);

})();
