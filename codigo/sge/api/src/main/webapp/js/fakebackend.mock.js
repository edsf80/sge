/**
 * @ngdoc mock
 * @name Sge API mock
 * 
 * @description Mock para simulação de backend
 * 
 * @author Edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	'use strict';

	var estadosDemanda = [ 'N', 'L', 'A', 'E', 'C', 'F' ];
	var transicoesDemanda = [ [ 'N', 'L' ], [ 'L', 'A', 'N' ],
			[ 'A', 'E', 'C' ], [ 'E', 'F', 'C' ], [ 'C' ], [ 'F', 'E' ] ];

	var app = angular.module('sge').run(setupFakeBackend);

	function searchTransicoesDemanda(estadoAtualDemanda) {
		for (var i = 0; i < estadosDemanda.length; i++) {
			if (estadoAtualDemanda == estadosDemanda[i]) {
				return transicoesDemanda[i];
			}
		}
	}

	// setup fake backend for backend-less development
	function setupFakeBackend($httpBackend) {
		var testUser = {
			username : 'test',
			password : 'test',
			firstName : 'Test',
			lastName : 'User'
		};

		$httpBackend.whenPOST('login').respond(
				function(method, url, data) {
					var params = angular.fromJson(data);

					// check user credentials and return fake jwt token if valid
					if (params.login === testUser.username && params.senha === testUser.password) {
						return [ 200, {
							nome : "Ednaldo Dilorenzo"
						}, {} ];
					} else {
						return [ 422, "Usuário ou senha inválida", {} ];
					}
				});

		// fake authenticate api end point
		$httpBackend.whenGET('usuario').respond(function(method, url, data) {

			return [ 200, {
				id : 1,
				nome : "Ednaldo Dilorenzo"
			} ];

		});

		$httpBackend.whenGET('logout').respond(function(method, url, data) {

			return [ 200, {} ];

		});

		$httpBackend.whenGET('/api/categoria').respond(
				function(method, url, data) {

					return [ 200, {
						token : 'fake-jwt-token',
						sucesso : true,
						dados : [ {
							id : 1,
							descricao : "Categoria Teste 1"
						}, {
							id : 2,
							descricao : "Categoria Teste 2"
						} ]
					} ];

				});

		$httpBackend.whenPOST('/api/categoria').respond(
				function(method, url, data) {

					var params = angular.fromJson(data);

					return [ 200, {
						sucesso : true,
						dados : {
							id : 1,
							descricao : 'Categoria Teste'
						}
					} ];
				});

		$httpBackend.whenPUT('/api/categoria').respond(
				function(method, url, data) {
					// get parameters from post request
					var params = angular.fromJson(data);

					return [ 200, {
						sucesso : true,
						dados : {
							id : params.id,
							descricao : params.descricao
						}
					} ];
				});

		$httpBackend.whenGET('demanda').respond(function(method, url, data) {

			return [ 200, [ {
				id : 1,
				descricao : "Demanda Teste 1",
				status : "N",
				transicoes : [ 'N', 'L' ]
			}, {
				id : 2,
				descricao : "Demanda Teste 2",
				status : "A",
				transicoes : [ 'A', 'E', 'C' ]
			}, {
				id : 3,
				descricao : "Demanda Teste 3",
				status : "E",
				transicoes : [ 'E', 'F', 'C' ]
			} ] ];

		});

		$httpBackend.whenPOST('demanda').respond(function(method, url, data) {
			var params = angular.fromJson(data);

			return [ 200, {
				id : 10,
				descricao : params.descricao,
				status : params.status,
				estimativa : params.estimativa,
				transicoes : searchTransicoesDemanda(params.status)
			} ];
		});

		$httpBackend.whenPUT('demanda').respond(function(method, url, data) {
			var params = angular.fromJson(data);

			return [ 200, {
				id : params.id,
				descricao : params.descricao,
				status : params.status,
				estimativa : params.estimativa,
				transicoes : searchTransicoesDemanda(params.status)
			} ];
		});

		$httpBackend.whenGET('nivel').respond(function(method, url, data) {

			return [ 200, [ {
				id : 1,
				descricao : "Testador"
			}, {
				id : 2,
				descricao : "Desenvolvedor"
			}, {
				id : 3,
				descricao : "Líder de Projeto"
			}, {
				id : 4,
				descricao : "Arquiteto"
			} ] ];
		});

		$httpBackend.whenPOST('nivel').respond(function(method, url, data) {
			var params = angular.fromJson(data);

			return [ 200, {
				id : params.id,
				descricao : params.descricao,
				matricula : params.matricula
			} ];
		});

		$httpBackend.whenDELETE('nivel').respond(function(method, url, data) {

			return [ 200, {
				id : 1,
				descricao : "teste",
				matricula : "matricula"
			} ];
		});

		$httpBackend.whenGET('aluno').respond(function(method, url, data) {

			return [ 200, [ {
				matricula : 2015261526152,
				nome : "José Rafael Remígio",
				dataInicio : "25/01/2016"
			}, {
				matricula : 201515020089,
				nome : "Franck Oliveira de Aragão Júnior",
				dataInicio : "2016-10-04T03:00:00.000Z"
			}, {
				matricula : 201515020089,
				nome : "Emerson Davi Alexandre",
				dataInicio : "2016-10-04T03:00:00.000Z"
			} ] ];

		});

		$httpBackend.whenGET('aluno/2015261526152').respond(
				function(method, url, data) {

					return [ 200, {
						id : 1,
						matricula : 2015261526152,
						nome : "Jose Rafael Remijo",
						admissoes : [ {
							id : 1,
							dataInicio : "2016-10-04T03:00:00.000Z"
						} ],
						niveis : [ {
							id : 1,
							descricao : "Testador"
						}, {
							id : 2,
							descricao : "Desenvolvedor"
						}, {
							id : 3,
							descricao : "Líder de Projeto"
						} ]
					} ];

				});

		$httpBackend.whenGET('aluno/201515020089').respond(
				function(method, url, data) {

					return [ 200, {
						id : 2,
						matricula : 201515020089,
						nome : "Franck Oliveira de Aragão Júnior",
						admissoes : [ {
							id : 1,
							dataInicio : "2016-10-04T03:00:00.000Z",
							dataTermino : "2016-10-10T03:00:00.000Z"
						} ],
						niveis : [ {
							id : 1,
							descricao : "Testador"
						}, {
							id : 2,
							descricao : "Desenvolvedor"
						} ]
					} ];

				});

		// SIMULAÇÃO DE DADOS PARA TREINAMENTO [INICIO]

		$httpBackend
				.whenGET('treinamento')
				.respond(
						function(method, url, data) {
							return [
									200,
									[
											{
												id : 1,
												titulo : "Treinamento Spring",
												descricao : "Apreder conceitos funcadamentais do Spring",
												conteudos : [
														{
															descricao : 'Curso Spring Boot',
															link : 'https://www.youtube.com/watch?v=XbknBOmMuPQ&list=PLGDwUiT1wr6-Fn3N2oqJpTdhGjFHnIIKY'
														},
														{
															descricao : 'Videos sobre Spring Data',
															link : 'https://www.youtube.com/watch?v=kbisNUfqVLM&list=PLGDwUiT1wr6-Fn3N2oqJpTdhGjFHnIIKY&index=2'
														} ]
											},
											{
												id : 2,
												titulo : "Treinamento Git",
												descricao : "Apreder a tarabalhar com controle de versão com GIT.",
												conteudos : [ {
													descricao : 'Git na prática - PlayList',
													link : 'youtube.com/frfr/git'
												} ]
											},
											{
												id : 3,
												titulo : "Treinamento JPA",
												descricao : "Aprender conceitos sobre jpa",
												conteudos : [ {
													descricao : 'JPA AlgaWorks - Livreto',
													link : 'algaworks.com/jpa'
												} ]
											},
											{
												id : 4,
												titulo : "Treinamento JPA  TESTE",
												descricao : "Aprender conceitos sobre jpa",
												conteudos : [ {
													descricao : 'JPA AlgaWorks - Livreto',
													link : 'https://www.youtube.com/watch?v=kbisNUfqVLM&list=PLGDwUiT1wr6-Fn3N2oqJpTdhGjFHnIIKY&index=2'
												} ]
											},

									] ];

						});

		$httpBackend
				.whenGET('treinamento/4')
				.respond(
						function(method, url, data) {

							return [
									200,
									{
										id : 4,
										titulo : "Treinamento Spring",
										descricao : "Apreder conceitos funcadamentais do Spring",
										conteudos : [
												{
													descricao : 'Curso Spring Boot',
													link : 'https://www.youtube.com/watch?v=kbisNUfqVLM&list=PLGDwUiT1wr6-Fn3N2oqJpTdhGjFHnIIKY&index=2'
												},
												{
													descricao : 'Videos sobre Spring Data',
													link : 'https://www.youtube.com/watch?v=XbknBOmMuPQ&list=PLGDwUiT1wr6-Fn3N2oqJpTdhGjFHnIIKY'
												} ]
									} ]
						});

		$httpBackend.whenPOST('treinamento').respond(
				function(method, url, data) {
					var params = angular.fromJson(data);

					return [ 200, {
						id : params.id,
						titulo : params.titulo,
						descricao : params.descricao,
						conteudo : params.conteudo
					} ];
				});

		$httpBackend.whenPUT('treinamento').respond(
				function(method, url, data) {
					var params = angular.fromJson(data);

					return [ 200, {
						id : params.id,
						titulo : params.titulo,
						descricao : params.descricao,
						conteudo : params.conteudo
					} ];
				});

		$httpBackend.whenDELETE('treinamento').respond(
				function(method, url, data) {

					return [ 200, {
						id : 1,
						titulo : "Treinamento Spring",
						descricao : "descrição",
						conteudo : {
							descricao : "desc",
							link : "lik.com/"
						}
					} ];
				});

		// SIMULAÇÃO DE DADOS PARA TREINAMENTO [FIM]

		$httpBackend.whenPOST('/api/admissao').respond(
				function(method, url, data) {
					// get parameters from post request
					var params = angular.fromJson(data);

					return [ 200, {
						sucesso : true,
						dados : {
							id : 10,
							dataInicio : params.dataInicio,
							dataTermino : params.dataTermino
						}
					} ];
				});

		$httpBackend.whenPUT('/api/admissao').respond(
				function(method, url, data) {
					var params = angular.fromJson(data);

					return [ 200, {
						sucesso : true,
						dados : {
							id : params.id,
							dataInicio : params.dataInicio,
							dataTermino : params.dataTermino
						}
					} ];
				});

		// pass through any urls not handled above so static files are served
		// correctly
		$httpBackend.whenGET(/^\w+.*/).passThrough();
	}

})();
