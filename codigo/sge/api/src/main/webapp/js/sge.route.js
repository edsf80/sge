/**
 * @ngdoc Route
 * @name sgeRoute
 * 
 * @description mapeamento de rotas usando ui-router com 
 * carregamento lazy de arquivos
 * 
 * @author Edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */

(function() {
	var app = angular.module('sgeRoute', [ 'ui.router', 'oc.lazyLoad' ]);

	app.config(function($stateProvider, $urlRouterProvider) {

				$urlRouterProvider.otherwise("/login");

				$stateProvider
						.state(
								'login',
								{
									url : "/login",
									data : {
										pageTitle : 'Login'
									},
									templateUrl : "components/public/login/index.view.html",
									controller : 'LoginController as loginCtrl'
								})
						.state(
								'home',
								{
									url : "/home",
									data : {
										pageTitle : 'Home'
									},
									templateUrl : "components/private/home/index.view.html",
									resolve : {
										loadMyDirectives : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'sgeHome',
														files : [
																'components/private/partials/header.directive.js',
																'components/private/partials/header-notification.directive.js',
																'components/private/partials/header-modal.component.js',
																'components/private/partials/help-block.component.js',
																'components/private/partials/sidebar.controller.js',
																'components/private/partials/page-header.directive.js',
																'components/private/partials/widget.js',
																'components/private/partials/widget-body.js',
																'components/private/services/nivelaluno.service.js']
													})
										}
									}
								})
						.state(
								'home.dashboard',
								{
									url : "/dashboard",
									data : {
										pageTitle : 'Dashboard'
									},
									templateUrl : "components/private/dashboard/index.view.html",
									resolve : {
										loadMyDirectives : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'dashboard',
														files : [ 'components/private/dashboard/index.controller.js' ]
													})
										}
									}
								})
						.state(
								'home.demanda',
								{
									url : "/demanda",
									data : {
										pageTitle : 'Demandas'
									},
									templateUrl : "components/private/demanda/index.view.html",
									controller : 'DemandaController as demandaCtrl',
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'demanda',
														files : [
																'components/private/demanda/index.controller.js',
																'components/private/demanda/modal.controller.js',
																'components/private/services/demanda.service.js' ]
													})
										}
									}
								})
						.state(
								'home.aluno',
								{
									url : "/aluno",
									data : {
										pageTitle : 'Alunos'
									},
									templateUrl : "components/private/aluno/index.view.html",
									controller : 'AlunoController as alunoCtrl',
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'aluno',
														files : [
																'components/private/aluno/index.controller.js',
																'components/private/services/aluno.service.js' ]
													})
										}
									}
								})
						.state(
								'home.admissao',
								{
									url : "/admissao/:matricula",
									data : {
										pageTitle : 'Admissões'
									},
									templateUrl : "components/private/admissao/index.view.html",
									controller : "AdmissaoController as admissaoCtrl",
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'admissao',
														files : [
																'components/private/admissao/index.controller.js',
																'components/private/admissao/modal.controller.js',
																'components/private/services/admissao.service.js',
																'components/private/services/nivel.service.js',
																'components/private/services/treinamento.service.js',
																'components/private/services/aluno.service.js']
													})
										}
									}
								})
						.state(
								'home.cliente',
								{
									url : "/cliente",
									data : {
										pageTitle : 'Clientes'
									},
									templateUrl : "components/private/cliente/index.view.html",
									controller : "ClienteController as clienteCtrl",
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'cliente',
														files : [
																'components/private/cliente/index.controller.js',
																'components/private/cliente/modal.controller.js',
																'components/private/services/cliente.service.js' ]
													})
										}
									}
								})
						.state(
								'home.nivel',
								{
									url : "/nivel",
									data : {
										pageTitle : 'Níveis'
									},
									templateUrl : "components/private/nivel/index.view.html",
									controller : "NivelController as nivelCtrl",
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'nivel',
														files : [
																'components/private/nivel/index.controller.js',
																'components/private/services/nivel.service.js',
																'components/private/nivel/modal.controller.js',
																'components/private/services/aluno.service.js']
													})
										}
									}
								})
						.state(
								'home.nivelAluno',
								{
									url : "/nivelAluno",
									data : {
										pageTitle : 'Meus Níveis'
									},
									templateUrl : "components/private/nivelAluno/index.view.html",
									controller : "NivelAlunoController as nivelAlunoCtrl",
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'nivelAluno',
														files : [
																'components/private/nivelAluno/index.controller.js',
																'components/private/services/aluno.service.js']
													})
										}
									}
								})
						.state(
								'home.detalheNivelAluno',
								{
									url : "/detalheNivelAluno/:detailNivelAluno",
									data : {
										pageTitle : 'Detalhes Nível'
									},
									templateUrl : "components/private/nivelAluno/index.view.detail.html",
									controller : "NivelAlunoDetailController as nivelADCtrl",
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'detalheNivelAluno',
														files : [
																'components/private/nivelAluno/index.detail.controller.js',
																'components/private/services/nivel.service.js',
																'components/private/services/treinamento.service.js' ]
													})
										}
									}
								})
						.state(
								'home.treinamento',
								{
									url : "/treinamento",
									data : {
										pageTitle : 'Treinamentos'
									},
									templateUrl : "components/private/treinamento/index.view.html",
									controller : "TreinamentoController as treinamentoCtrl",
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'treinamento',
														files : [
																'components/private/treinamento/index.controller.js',
																'components/private/services/treinamento.service.js',
																'components/private/services/prova.service.js' ]
													})
										}
									}
								})
						.state(
								'home.treinamentoEdit',
								{
									url : "/treinamentoEdit",
									data : {
										pageTitle : 'Edição Treinamento'
									},
									templateUrl : "components/private/treinamento/index.edit.view.html",
									controller : "TreinamentoEditController as treinamentoECtrl",
									params : {
										treinamentoEdit : null
									},
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'treinamentoEdit',
														files : [
																'components/private/treinamento/index.edit.controller.js',
																'components/private/services/treinamento.service.js',
																'components/private/services/nivel.service.js',
																'components/private/services/prova.service.js' ]
													})
										}
									}
								})
						.state(
								'home.treinamentoDetail',
								{
									url : "/treinamentoDetail/:treinamentoDetail",
									data : {
										pageTitle : 'Detalhes Treinamento'
									},
									templateUrl : "components/private/treinamento/index.detail.view.html",
									controller : "TreinamentoDetailController as treinamentoDCtrl",
									reloadOnSearch: false,
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'treinamentoDetalhe',
														files : [ 'components/private/treinamento/index.detail.controller.js' ]
													})
										}
									}
								})
						.state(
								'home.prova',
								{
									url : "/prova",
									data : {
										pageTitle : 'Provas'
									},
									templateUrl : "components/private/prova/index.view.html",
									controller : "provaController as provaCtrl",
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'prova',
														files : [
																'components/private/prova/index.controller.js',
																'components/private/services/prova.service.js' ]
													})
										}
									}
								})
						.state(
								'home.provaEdit',
								{
									url : "/provaEdit",
									data : {
										pageTitle : 'Edição de Prova'
									},
									templateUrl : "components/private/prova/index.edit.view.html",
									controller : "ProvaEditController as provaEditCtrl",
									params : {
										provaEdit : null
									},
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'provaEdit',
														files : [
																'components/private/prova/index.edit.controller.js',
																'components/private/prova/modal.controller.js',
																'components/private/services/prova.service.js' ]
													})
										}
									}
								})
						.state(
								'home.detalheProva',
								{
									url : "/detalheProva/:detailProva",
									data : {
										pageTitle : 'Realizar Prova'
									},
									templateUrl : "components/private/prova/index.detail.view.html",
									controller : "ProvaDetailController as provaDCtrl",
									reloadOnSearch: false,
									resolve : {
										loadMyFiles : function($ocLazyLoad) {
											return $ocLazyLoad
													.load({
														name : 'detalheProva',
														files : [ 'components/private/prova/index.detail.controller.js',
														          'components/private/services/treinamento-alocado.service.js']
													})
										}
									}
								});
			});
})();
