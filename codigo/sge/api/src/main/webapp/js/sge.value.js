/**
 * @ngdoc Value
 * @name Sge Values
 * 
 * @description value para dados estaticos
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 */
(function() {
	angular.module("sge").value("sgeValue", {

		prova: {
			descricao : 'CLICK PARA ADICIONAR UM ENUNCIADO',
		}
	});
})();