/**
 * 
 */
package br.com.hyperactive.sge.domain.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @author edsf
 *
 */
/**
 * @author edsf
 *
 */
public class Aluno implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3187982784420007974L;

	/**
	 * 
	 */
	private String matricula;

	/**
	 * 
	 */
	private String nome;

	/**
	 * 
	 */
	private String email;

	/**
	 * 
	 */
	private Conta conta;

	/**
	 * 
	 */
	private List<Admissao> admissoes;

	/**
	 * 
	 */
	private List<NivelAluno> niveis;

	/**
	 * @return
	 */
	public List<Admissao> getAdmissoes() {
		return admissoes;
	}

	/**
	 * @param admissoes
	 */
	public void setAdmissoes(List<Admissao> admissoes) {
		this.admissoes = admissoes;
	}

	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}

	/**
	 * @param matricula
	 *            the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public Conta getConta() {
		return conta;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setConta(Conta conta) {
		this.conta = conta;
	}

	/**
	 * @return the niveis
	 */
	public List<NivelAluno> getNiveis() {
		return niveis;
	}

	/**
	 * @param niveis
	 *            the niveis to set
	 */
	public void setNiveis(List<NivelAluno> niveis) {
		this.niveis = niveis;
	}

}
