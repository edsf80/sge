package br.com.hyperactive.sge.domain.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entidade de négocio que representa um Treinamento. Para cada nível que o
 * aluno entra ele deve fazer o treinamento deste nível.
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@Entity
@Table(name = "treinamento")
public class Treinamento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	private String titulo;

	private String descricao;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Conteudo> conteudos;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	private Prova prova;

	/**
	 * Tempo necessário para conclusão do treinamento em dias
	 */
	@Column(name = "tempo_treinamento")
	private Integer tempoTreinamento;

	@ManyToOne(cascade = CascadeType.MERGE)
	private Nivel nivel;

	public Treinamento() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Conteudo> getConteudos() {
		return conteudos;
	}

	public void setConteudos(List<Conteudo> conteudos) {
		this.conteudos = conteudos;
	}

	public Prova getProva() {
		return prova;
	}

	public void setProva(Prova prova) {
		this.prova = prova;
	}

	/**
	 * @return the tempoTreinamento
	 */
	public Integer getTempoTreinamento() {
		return tempoTreinamento;
	}

	/**
	 * @param tempoTreinamento
	 *            the tempoTreinamento to set
	 */
	public void setTempoTreinamento(Integer tempoTreinamento) {
		this.tempoTreinamento = tempoTreinamento;
	}


	/**
	 * @return the nivel
	 */
	public Nivel getNivel() {
		return nivel;
	}

	/**
	 * @param nivel
	 *            the nivel to set
	 */
	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}

}
