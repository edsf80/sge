/**
 * 
 */
package br.com.hyperactive.sge.domain.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

/**
 * @author edsf
 *
 */
@Entity
@SequenceGenerator(name = "seq_admissao", initialValue = 1, allocationSize = 1)
public class Admissao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7784316565471954266L;
	
	@Id
	@Column(name = "id", unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_admissao")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column
	@NotNull(message = "Admissão deve ser associada a um aluno")
	private String matricula;

	@Column
	@NotNull(message = "Admissão deve conter data de início")
	private Date dataInicio;

	@Column
	private Date dataTermino;

	
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}

}
