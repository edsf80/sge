/**
 * 
 */
package br.com.hyperactive.sge.domain.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @author edsf
 *
 */
public class Conta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -25123773785049615L;

	private String login;

	private String senha;

	private List<String> papeis;

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login
	 *            the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * @param senha
	 *            the senha to set
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * @return the papeis
	 */
	public List<String> getPapeis() {
		return papeis;
	}

	/**
	 * @param papeis
	 *            the papeis to set
	 */
	public void setPapeis(List<String> papeis) {
		this.papeis = papeis;
	}

}
