/**
 * 
 */
package br.com.hyperactive.sge.domain.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

/**
 * @author edsf
 *
 */
@Entity
@SequenceGenerator(name = "seq_demanda", initialValue = 1, allocationSize = 1)
public class Demanda implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -132563953388035287L;

	private transient char[] estados = { 'N', 'L', 'A', 'E', 'C', 'F' };

	private transient char[][] transicoesEstados = { { 'N', 'L' }, { 'L', 'A', 'N' }, { 'A', 'E', 'C' },
			{ 'E', 'F', 'C' }, { 'C' }, { 'F', 'E' } };

	@Id
	@Column(name = "id", unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_demanda")
	private Long id;

	@NotNull(message = "Descrição da demanda deve ser informada")
	@Column
	private String descricao;

	@Column
	@NotNull(message = "Status da demanda deve ser informado")
	private Character status;

	@Column
	private byte estimativa;

	@Column
	private Date dataInicio;

	@Column
	private Date dataTermino;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public byte getEstimativa() {
		return estimativa;
	}

	public void setEstimativa(byte estimativa) {
		this.estimativa = estimativa;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}

	public char[] getTransicoes() {
		char[] resultado = {};

		if (this.getStatus() == 0) {
			return transicoesEstados[0];
		} else {
			for (int i = 0; i < estados.length; i++) {
				if (this.getStatus() == estados[i]) {
					return transicoesEstados[i];
				}
			}
		}

		return resultado;
	}

}
