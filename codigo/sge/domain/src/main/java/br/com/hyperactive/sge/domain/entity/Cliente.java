/**
 * 
 */
package br.com.hyperactive.sge.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

/**
 * @author edsf
 *
 */
@Entity
@SequenceGenerator(name = "seq_cliente", initialValue = 1, allocationSize = 1)
public class Cliente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -979657106306730130L;

	@Id
	@Column(name = "id", unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_cliente")
	private Long id;

	@NotNull(message = "Descrição do cliente deve ser informada")
	@Column
	private String descricao;

	@NotNull(message = "CPF/CNPJ do cliente deve ser informado")
	@Column
	private String codigo;

	@NotNull(message = "Tipo do cliente deve ser informado")
	@Column
	private char tipo;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getDescricao() {
		return this.descricao;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the tipo
	 */
	public char getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

}
