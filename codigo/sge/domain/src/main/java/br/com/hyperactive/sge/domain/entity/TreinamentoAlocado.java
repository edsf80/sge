package br.com.hyperactive.sge.domain.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@Entity
public class TreinamentoAlocado {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	private Boolean finalizado;

	private String matricula;

	@OneToOne(cascade = CascadeType.MERGE)
	private Treinamento treinamento;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the finalizado
	 */
	public Boolean getFinalizado() {
		return finalizado;
	}

	/**
	 * @param finalizado
	 *            the finalizado to set
	 */
	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}

	/**
	 * @param matricula
	 *            the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	/**
	 * @return the treinamento
	 */
	public Treinamento getTreinamento() {
		return treinamento;
	}

	/**
	 * @param treinamento
	 *            the treinamento to set
	 */
	public void setTreinamento(Treinamento treinamento) {
		this.treinamento = treinamento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TreinamentoAlocado [id=" + id + ", finalizado=" + finalizado + ", matricula=" + matricula
				+ ", treinamento=" + treinamento + "]";
	}

}
