package br.com.hyperactive.sge.domain.enumerations;

/**
 * 
 * 
 * <p>
 * <b>Nivel de Dificuldade</b>
 * </p>
 *
 * <p>
 * 	Enum representa os níveis de dificuldade de uma @Questao.
 * </p>
 *
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>	
 *
 */
public enum NivelDificuldade {
	
	FACIL, MEDIO, DIFICIL;
	
}
