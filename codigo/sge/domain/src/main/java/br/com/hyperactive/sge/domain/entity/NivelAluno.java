/**
 * 
 */
package br.com.hyperactive.sge.domain.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * @author edsf
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@Entity
public class NivelAluno {

	/**
	 * 
	 */
	@Id
	@Column(name = "id_nivel_aluno", unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	/**
	 * 
	 */
	@Column
	@NotNull(message = "Matrícula deve ser informada")
	private String matricula;

	/**
	 * 
	 */
	@ManyToOne
	@JoinColumn(name = "id_nivel", nullable = false)
	@NotNull(message = "Nível deve ser informado")
	private Nivel nivel;

	@OneToMany(cascade = CascadeType.PERSIST, orphanRemoval = true)
	private List<TreinamentoAlocado> treinamentos;

	@Temporal(TemporalType.TIMESTAMP)
	private Date prazoConclusao;

	@Transient
	private Boolean concluido = false;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}

	/**
	 * @param matricula
	 *            the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	/**
	 * @return the nivel
	 */
	public Nivel getNivel() {
		return nivel;
	}

	/**
	 * @param nivel
	 *            the nivel to set
	 */
	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}

	/**
	 * @return the prazoConclusao
	 */
	public Date getPrazoConclusao() {
		return prazoConclusao;
	}

	/**
	 * @param prazoConclusao
	 *            the prazoConclusao to set
	 */
	public void setPrazoConclusao(Date prazoConclusao) {
		this.prazoConclusao = prazoConclusao;
	}

	/**
	 * @return the treinamentos
	 */
	public List<TreinamentoAlocado> getTreinamentos() {
		return treinamentos;
	}

	/**
	 * @param treinamentos
	 *            the treinamentos to set
	 */
	public void setTreinamentos(List<TreinamentoAlocado> treinamentos) {
		this.treinamentos = treinamentos;
	}

	/**
	 * @return the concluido
	 */
	public Boolean getConcluido() {
		int ntreinamentosFinalizados = 0;
		if(this.treinamentos != null) {
			for (TreinamentoAlocado treinamentoAlocado : treinamentos) {
				if(treinamentoAlocado.getFinalizado()) {
					ntreinamentosFinalizados ++;
				}
			}
			
			if(ntreinamentosFinalizados > 0 && ntreinamentosFinalizados == this.treinamentos.size()) {
				this.concluido = true;
			}
		}
		return concluido;
	}

	/**
	 * @param concluido
	 *            the concluido to set
	 */
	public void setConcluido(Boolean concluido) {
		this.concluido = concluido;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NivelAluno [id=" + id + ", matricula=" + matricula + ", nivel=" + nivel + ", prazoConclusao="
				+ prazoConclusao + "]";
	}

}
