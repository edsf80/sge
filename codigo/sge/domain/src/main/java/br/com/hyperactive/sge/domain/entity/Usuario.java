/**
 * 
 */
package br.com.hyperactive.sge.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 * @author edsf
 *
 */
@Entity
@SequenceGenerator(name = "seq_usuario", initialValue = 1, allocationSize = 100)
public class Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9174598286276674155L;

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_usuario")
	private Long id;
	
	/**
	 * 
	 */
	@Column
	private String nome;
	
	/**
	 * 
	 */
	@Column
	private String login;
	
	/**
	 * 
	 */
	@Column
	private String senha;

	/**
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
