package br.com.hyperactive.sge.domain.enumerations;

/**
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>	
 *
 */
public enum TipoConteudo {
	
	VIDEO, DOCUMENTO

}
